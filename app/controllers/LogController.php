<?php

class LogController extends Controller {

    function getTechnicianWork($technicianId) {
        $arrayTechnicianTasksTaskIdByTechnicianId = $this->getTechnicianTasksDistinctTasksId($technicianId);
        foreach ($arrayTechnicianTasksTaskIdByTechnicianId as $technicianTask) {
            echo "<h2>" . $this->getProjectName($technicianTask['task_id']) . "</h2>";
            echo "<h2>" . $this->getTaskName($technicianTask['task_id']) . "</h2>";
            print_r($this->getTechnicianTaskByTaskAndTechnicianId($technicianId, $technicianTask['task_id']));
        }
    }

    function getTaskName($taskId) {
        $taskController = new TaskController();
        return $taskController->getTaskName($taskId);
    }

    function getProjectName($taskId) {
        $taskController = new TaskController();
        $task = $taskController->getTask($taskId);
        $projectController = new ProjectController();
        return $projectController->getProjectName($task['project_id']);
    }

    //genera un array con los id(distinct) del tabla en la BD "v2_technician_task"
    function getTechnicianTasksDistinctTasksId($technicianId) {
        $technicianTaskController = new TechnicianTaskController();
        return $technicianTaskController->getAllDistinctTaskIdOfTechnician($technicianId);
    }

    function getTechnicianHours($technicianId) {
        $technicianTaskController = new TechnicianTaskController();
        $arrayTechnicianTasks = $technicianTaskController->getAllTechnicianTechnicianTasks($technicianId);
        $minutes = 0;
        foreach ($arrayTechnicianTasks as $technicianTask) {
            $minutes += $technicianTask['minutes'];
        }
        return Util::convertToHoursMins($minutes);
    }

    //dados el technician id y task id crea un array con los technicianTasks
    function getTechnicianTaskByTaskAndTechnicianId($technicianId, $taskId) {
        $technicianTaskController = new TechnicianTaskController();
        return $technicianTaskController->getTechnicianTasksBy2Clauses('technician_id', $technicianId, 'task_id', $taskId);
    }

    //dados el technician id y task id devuelve el total de horas relativas a la tarea
    function getTechnicianTaskByTaskAndTechnicianIdHours($technicianId, $taskId) {
        $technicianTaskController = new TechnicianTaskController();
        $arrayTechnicianTasks = $this->getTechnicianTaskByTaskAndTechnicianId($technicianId, $taskId);
        $minutes = 0;
        foreach ($arrayTechnicianTasks as $technicianTask) {
            $minutes += $technicianTask['minutes'];
        }
        return Util::convertToHoursMins($minutes);
    }

    //returns associative array with info of the projects a technician is working on by id (parameter)
    function getTechnicianTasksFullInfo($technicianId) {
        $technicianTask = new TechnicianTask();
        $technicianWork = array();
        foreach ($technicianTask->getAllTechnicianTasks() as $technicianTaskInfo) {
            if ($technicianTaskInfo['technician_id'] == $technicianId) {
                array_push($technicianWork, $this->createProjectFullInfo($technicianTaskInfo));
            }
        }
        return $technicianWork;
    }

    function createProjectFullInfo(array $technicianTask) {
        $task = new Task();
        $taskInfo = $task->getTaskById($technicianTask['task_id']);
        $project = new Project();
        $projectInfo = $project->getProjectById($taskInfo['project_id']);

        $workInfo = array(
            'project' => $projectInfo,
            'task' => $taskInfo,
            'technicianTask' => $technicianTask
        );
        return $workInfo;
    }

    //returns associative array with info of a project by id (parameter)    
    function getProjectTasks($projectId) {
        $project = new Project();
        $task = new Task();
        $projectWork = array();
        foreach ($task->getAllTasks() as $taskInfo) {
            if ($taskInfo['project_id'] == $projectId) {
                array_push($projectWork, $this->createTaskFullInfo($taskInfo));
            }
        }
        return $projectWork;
    }

    //returns project tasks hours summation *
    function getProjectTasksHours($projectId) {
        $taskController = new TaskController();
        $minutes = 0;
        $tasks = $taskController->getWholeTasksByProjectId($projectId);
        foreach ($tasks as $task) {
            $minutes += $taskController->getTaskHours($task['id']);
        }
        return Util::convertToHoursMins($minutes);
    }

    function createTaskFullInfo(array $task) {
        $technicianTask = new TechnicianTask();
        $technicianTasks = array();
        $minutes = 0;
        foreach ($technicianTask->getAllTechnicianTasks() as $technicianTaskInfo) {
            if ($technicianTaskInfo['task_id'] == $task['id']) {
                array_push($technicianTasks, $technicianTaskInfo);
                $minutes += $technicianTaskInfo['minutes'];
            }
        }
        $taskFullInfo = array(
            'task' => $task,
            'technicianTasks' => $technicianTasks,
            'hours' => Util::convertToHoursMins($minutes)
        );

        return $taskFullInfo;
    }

    function getLogByDate($startDate, $finishDate) {
        $taskController = new TaskController();
        $technicianTaskController = new TechnicianTaskController();
        $projectController = new ProjectController();
        $tasksIdArray = Util::delete_duplicates_in_array_by_key($technicianTaskController->getTaskIdsInDateRange($startDate, $finishDate), 'task_id');
        $defArray = array();
        foreach ($tasksIdArray as $task) {
            $technicianTasks = $technicianTaskController->getTechnicianTasksOnDateRangeWhereTaskId($startDate, $finishDate, $task['task_id']);
            $task = $taskController->getTask($task['task_id']);
            $projectName = $projectController->getProjectName($task['project_id']);
            $wholeInfo = array("$projectName" => array("projectName" => $projectName, "task" => $task, "technicianTasks" => $technicianTasks));
            if (!Util::checkIfExists($defArray, 'projectName', $projectName)) {
                array_push($defArray, $wholeInfo);
            } else {
                //array_push($defArray, $projectName);
            }
        }
        return $defArray;
    }

    function getLogByDate2($startDate, $finishDate) {
        $taskController = new TaskController();
        $technicianTaskController = new TechnicianTaskController();
        $projectController = new ProjectController();

        $tasksIdArray = Util::delete_duplicates_in_array($technicianTaskController->getTaskIdsInDateRange($startDate, $finishDate));
        $projectsId = array();

        foreach ($tasksIdArray as $taskId) {
            $task = $taskController->getTask($taskId);
            array_push($projectsId, $task['project_id']);
        }

        $tasksArray = array();

        foreach ($tasksIdArray as $taskId) {
            $task = $taskController->getTask($taskId);
            array_push($tasksArray, $task);
        }
        $defArray = array();

        foreach (Util::delete_duplicates_in_array($projectsId) as $projectId) {
            $projectName = $projectController->getProjectName($projectId);
            $fullInfo = array("projectName" => $projectName, "taskInfo" => array());
            foreach ($taskController->getProjectTasksMatchingTaskId($projectId, $tasksIdArray) as $task) {
                array_push($fullInfo['taskInfo'], array("taskTitle" => $task['title'], "task_id" => $task['id'], "technicianTasks" => $technicianTaskController->getTechnicianTasksOnDateRangeWhereTaskId($startDate, $finishDate, $task['id'])));
            }
            array_push($defArray, $fullInfo);
        }

        return $defArray;
    }

    function getLogByTechnician($technicianId) {
        $taskController = new TaskController();
        $technicianTaskController = new TechnicianTaskController();
        $projectController = new ProjectController();
        $logController = new LogController();

        $tasksIdArray = $logController->getTechnicianTasksDistinctTasksId($technicianId);
        $projectsId = array();

        foreach ($tasksIdArray as $taskId) {
            $task = $taskController->getTask($taskId);
            array_push($projectsId, $task['project_id']);
        }

        $tasksArray = array();

        foreach ($tasksIdArray as $taskId) {
            $task = $taskController->getTask($taskId['task_id']);
            array_push($tasksArray, $task);
        }
        $defArray = array();

        foreach (Util::delete_duplicates_in_array($projectsId) as $projectId) {
            $projectName = $projectController->getProjectName($projectId);
            $fullInfo = array("projectName" => $projectName, "taskInfo" => array());
            foreach ($taskController->getProjectTasksMatchingTaskId($projectId, $tasksIdArray) as $task) {
                array_push($fullInfo['taskInfo'], array("taskTitle" => $task['title'], "task_id" => $task['id'], "technicianTasks" => $technicianTaskController->getTechnicianTaskByIdAndTaskId($task['id'], $technicianId)));
            }
            array_push($defArray, $fullInfo);
        }

        return $defArray;
    }

}
