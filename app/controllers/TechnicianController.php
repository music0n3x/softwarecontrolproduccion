<?php

class TechnicianController extends Controller {

    //returns record info by id (parameter) in an associative array
    function getTechnician($id) {
        $technician = new Technician();
        return $technician->getTechnicianById($id);
    }

    //returns all table records in an associative array
    function getAllTechnicians() {
        $technician = new Technician();
        return $technician->getAllTechnician();
    }
    
//returns all table records in an associative array ordered alphabetically
    function getAllTechniciansAlphabetically() {
        $technician = new Technician();
        return $technician->getAllTechnicianAlphabetically();
    }

    //inserts new record on database
    function createTechnician(array $technicianInfo) {
        $technician = new Technician();
        $technician->setName($technicianInfo['name']);
        $technician->setUsername($technicianInfo['username']);
        $technician->setPassword($technicianInfo['password']);
        $technician->setPrivilegesId($technicianInfo['privilegesId']);
        return $technician->createTechnician();
    }

    //inserts new record on database with id
    function createTechnicianWithId(array $technicianInfo) {
        $technician = new Technician();
        $technician->setId($technicianInfo['id']);
        $technician->setName($technicianInfo['name']);
        $technician->setUsername($technicianInfo['username']);
        $technician->setPassword($technicianInfo['password']);
        $technician->setPrivilegesId($technicianInfo['privilegesId']);
        return $technician->createTechnicianWithId();
    }

    //deletes database record by id (parameter)
    function deleteTechnician($id) {
        $technician = new Technician();
        $technician->deleteTechnicianById($id);
    }

    //updates record info
    function updateTechnician(array $technicianInfo) {
        $technician = new Technician();
        $technician->setId($technicianInfo['id']);
        $technician->setName($technicianInfo['name']);
        $technician->setUsername($technicianInfo['username']);
        $technician->setPassword($technicianInfo['password']);
        $technician->setPrivilegesId($technicianInfo['privilegesId']);
        $technician->editTechnician();
    }

    public static function getTechnicianName($id) {
        $technician = new Technician();
        $technicianInfo = $technician->getTechnicianById($id);
        return $technicianInfo['name'];
    }

    public function getTechnicianPasswordMD5($username, $password) {
        $technician = new Technician();
        return $technician->getPasswordMD5($username, $password);
    }

    public function getTechnicianByUsername($username) {
        $technician = new Technician();
        $technicianSearched = $technician->getAllByAlt('username', $username);
        if (!empty($technicianSearched)) {
            return $technicianSearched[0];
        }
    }

    /*
     * Creates data array
     * with    id -> update
     * without id -> insert
     */

    function createDataArray($name, $username, $password, $privilegesId, $id = null) {
        return array(
            'id' => $id,
            'name' => $name,
            'username' => $username,
            'password' => $password,
            'privilegesId' => $privilegesId
        );
    }

    function migrateTechnician() {
        foreach ($this->getFullMigrationData('tecnics') as $technician) {
            $technicianData = $this->createDataArray($technician['nom'], $technician['usuari'], $technician['password'], $technician['nivell'], $technician['idtecnic']);
            if (!$this->getTechnician($technician['idtecnic'])) {
                $this->createTechnicianWithId($technicianData);
                echo Util::recordActionLog('technician', $technician['idtecnic'], 'create');
            } else {
                $this->updateTechnician($technicianData);
                echo Util::recordActionLog('technician', $technician['idtecnic'], 'update');
            }
        }
    }

}
