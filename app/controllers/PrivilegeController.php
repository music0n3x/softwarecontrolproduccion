<?php

class PrivilegeController extends Controller {

    //returns privilege info by id (parameter) in an associative array
    function getPrivilege($id) {
        $privilege = new Privilege();
        return $privilege->getPrivilegeById($id);
    }   
    
    //returns all table records in an associative array 
    function getAllPrivileges() {
        $privilege = new Privilege();
        return $privilege->getAllPrivileges();
    }

    //inserts new record on database
    function createPrivilege(array $billingInfo) {
        $privilege = new Privilege();
        $privilege->setLevel($billingInfo['level']);
        return $privilege->createPrivilage();
    }

    //inserts new record on database
    function createPrivilegeWithId(array $privilegeInfo) {
        $privilege = new Privilege();
        $privilege->setId($privilegeInfo['id']);
        $privilege->setLevel($privilegeInfo['level']);
        return $privilege->createPrivilegeWithId();
    }

    //deletes database record by id (parameter)
    function deletePrivilege($id) {
        $privilege = new Privilege();
        $privilege->deletePrivilegeById($id);
    }

    //updates record info
    function updatePrivilege(array $privilegeInfo) {
        $privilege = new Privilege();
        $privilege->setId($privilegeInfo['id']);
        $privilege->setLevel($privilegeInfo['level']);
        $privilege->editPrivilege();
    }

    /*
     * Creates data array
     * with    id -> update
     * without id -> insert
     */

    function createDataArray($level, $id = null) {
        return array(
            'id' => $id,
            'level' => $level
        );
    }

}
