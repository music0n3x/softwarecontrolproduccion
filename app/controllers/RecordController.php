<?php

class RecordController extends Controller {

    //returns record info by id (parameter) in an associative array
    function getRecord($id) {
        $record = new Record();
        return $record->getRecordById($id);
    }

    //returns all table records in an associative array 
    function getAllRecords() {
        $record = new Record();
        return $record->getAllRecords();
    }

    //inserts new record on database
    function createRecord(array $recordInfo) {
        $record = new Record();
        $record->setFinishDate($recordInfo['finishDate']);
        $record->setStartDate($recordInfo['startDate']);
        $record->setTechnicianId($recordInfo['technicianId']);
        return $record->createRecord();
    }

    //deletes database record by id (parameter)
    function deleteRecord($id) {
        $record = new Record();
        $record->deleteRecordById($id);
    }

    //updates record info
    function updateRecord(array $recordInfo) {
        $record = new Record();
        $record->setId($recordInfo['id']);
        $record->setFinishDate($recordInfo['finishDate']);
        $record->setStartDate($recordInfo['startDate']);
        $record->setTechnicianId($recordInfo['technicianId']);
        $record->editRecord();
    }

    //returns every single record of a technian in an associative array
    function getTechnicianFullRecord($technicianId) {
        $record = new Record();
        $technicianRecord = array();
        foreach ($record->getAllRecords() as $recordInfo) {
            if ($recordInfo['technician_id'] == $technicianId) {
                array_push($technicianRecord, $recordInfo);
            }
        }
        return $technicianRecord;
    }

    /*
     * Creates data array
     * with    id -> update
     * without id -> insert
     */

    function createDataArray($finishDate, $startDate, $technicianId, $id = null) {
        return array(
            'id' => $id,
            'finishDate' => $finishDate,
            'startDate' => $startDate,
            'technicianId' => $technicianId
        );
    }
    
}
