<?php

class ProjectController extends Controller {

    //returns record info by id (parameter) in an associative array
    function getProjectInfo($id) {
        $project = new Project();
        return $project->getProjectById($id);
    }

    function getProjectName($id){
        $project = $this->getProjectInfo($id);
        return $project['name'];
    }
    
    //returns all table records in an associative array 
    function getAllProjects() {
        $project = new Project();
        return $project->getAllProjects();
    }
    
    //returns all table records in an associative array ordered alphabetically
    function getAllProjectsAlphabetically() {
        $project = new Project();
        return $project->getAllProjectsAlphabetically();
    }

    //inserts new record on database
    function createProject(array $projectInfo) {
        $project = new Project();
        $project->setName(Util::formatString($projectInfo['name']));
        $project->setInfo(Util::formatString($projectInfo['info']));
        $project->setCreatedDate($projectInfo['createdDate']);
        $project->setActive($projectInfo['active']);
        $project->setClientId($projectInfo['clientId']);
        return $project->createProject();
    }

    //inserts new record on database
    function createProjectWithId(array $projectInfo) {
        $project = new Project();
        $project->setId($projectInfo['id']);
        $project->setName(Util::formatString($projectInfo['name']));
        $project->setInfo(Util::formatString($projectInfo['info']));
        $project->setCreatedDate($projectInfo['createdDate']);
        $project->setActive($projectInfo['active']);
        $project->setClientId($projectInfo['clientId']);
        return $project->createProjectWithId();
    }

    //deletes database record by id (parameter)
    function deleteProject($id) {
        $project = new Project();
        $project->deleteProjectById($id);
    }

    //updates record info
    function updateProjectData(array $projectInfo) {
        $project = new Project();
        $project->setId($projectInfo['id']);
        $project->setName(Util::formatString($projectInfo['name']));
        $project->setInfo(Util::formatString($projectInfo['info']));
        $project->setCreatedDate($projectInfo['createdDate']);
        $project->setActive($projectInfo['active']);
        $project->setClientId($projectInfo['clientId']);
        $project->editProject();
    }

    //returns every single project of a client in an associative array
    function getClientProjects($idClient) {
        $project = new Project();
        $clientProjects = array();
        foreach ($project->getAllProjects() as $projectInfo) {
            if ($projectInfo['client_id'] == $idClient) {
                array_push($clientProjects, $projectInfo);
            }
        }
        return $clientProjects;
    }

    /*
     * Creates data array
     * with    id -> update
     * without id -> insert
     */

    function createDataArray($name, $info, $createdDate, $active, $clientId, $id = null) {
        return array(
            'id' => $id,
            'name' => $name,
            'info' => $info,
            'createdDate' => $createdDate,
            'active' => $active,
            'clientId' => $clientId
        );
    }

    function migrateProject() {
        foreach ($this->getFullMigrationData('projectes') as $project) {
            $projectData = $this->createDataArray($project['p_nom'], $project['p_observacions'], $project['fecha_projecte'], $project['activo'], $project['idclient'], $project['idprojecte']);
            if (!$this->getProjectInfo($project['idprojecte'])) {
                $this->createProjectWithId($projectData);
                echo Util::recordActionLog('project', $project['idprojecte'], 'create');
            } else {
                $this->updateProjectData($projectData);
                echo Util::recordActionLog('project', $project['idprojecte'], 'update');
            }
        }
    }

    function getAllDistinctIds() {
        $project = new Project();
        return $project->getAllDistinct('id');
    }
    
    function getProjectTasksOnDateRange(){
        
    }

}
