<?php

class ClientController extends Controller {

    //returns record info by id (parameter) in an associative array
    function getClientInfo($id) {
        $client = new Client();
        return $client->getClientById($id);
    }

    //returns all table records in an associative array  
    function getAllClients() {
        $client = new Client();
        return $client->getAllClients();
    }

    //inserts new record on database
    function createClient(array $clientInfo) {
        $client = new Client();
        $client->setName(Util::formatString($clientInfo['name']));
        $client->setInfo(Util::formatString($clientInfo['info']));
        return $client->createClient();
    }

    //inserts new record on database with id
    function createClientWithId(array $clientInfo) {
        $client = new Client();
        $client->setId($clientInfo['id']);
        $client->setName(Util::formatString($clientInfo['name']));
        $client->setInfo(Util::formatString($clientInfo['info']));
        return $client->createClientWithId();
    }

    //deletes database record by id (parameter)
    function deleteClient($id) {
        $client = new Client();
        $client->deleteClientById($id);
    }

    //updates record info
    function updateClientData(array $clientInfo) {
        $client = new Client();
        $client->setId($clientInfo['id']);
        $client->setName(Util::formatString($clientInfo['name']));
        $client->setInfo(Util::formatString($clientInfo['info']));
        $client->editClient();
    }

    /*
     * Creates data array
     * with    id -> update
     * without id -> insert
     */

    function createDataArray($name, $info, $id = null) {
        return array(
            'id' => $id,
            'name' => $name,
            'info' => $info
        );
    }

    function clientMigration() {
        foreach ($this->getFullMigrationData('clients') as $client) {
            $clientData = $this->createDataArray($client['c_nom'], $client["c_observacions"], $client['idclient']);
            if (!$this->getClientInfo($client['idclient'])) {
                $this->createClientWithId($clientData);
                echo Util::recordActionLog('client', $client['idclient'], 'create');
            } else {
                $this->updateClientData($clientData);
                echo Util::recordActionLog('client', $client['idclient'], 'update');
            }
        }
    }

}
