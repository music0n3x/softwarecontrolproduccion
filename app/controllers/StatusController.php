<?php

class StatusController extends Controller {

    //returns billing info by id (parameter) in an associative array
    function getStatus($id) {
        $status = new Status();
        return $status->getStatusById($id);
    }

    //returns all table records in an associative array 
    function getAllStatus() {
        $status = new Status();
        return $status->getAllStatus();
    }

    //inserts new record on database
    function createStatus(array $statusInfo) {
        $status = new Status();
        $status->setDescription($statusInfo['description']);
        return $status->createStatus();
    }

    //inserts new record on database
    function createStatusWithId(array $statusInfo) {
        $status = new Status();
        $status->setId($statusInfo['id']);
        $status->setDescription($statusInfo['description']);
        return $status->createStatusWithId();
    }

    //deletes database record by id (parameter)
    function deleteStatus($id) {
        $status = new Status();
        $status->deleteStatusById($id);
    }

    //updates record info
    function updateStatus(array $statusInfo) {
        $status = new Status();
        $status->setId($statusInfo['id']);
        $status->setDescription($statusInfo['description']);
        $status->editStatus();
    }

    /*
     * Creates data array
     * with    id -> update
     * without id -> insert
     */

    function createDataArray($description, $id = null) {
        return array(
            'id' => $id,
            'description' => $description
        );
    }

    function migrateStatus() {
        foreach ($this->getFullMigrationData('estat') as $status) {
            $statusData = $this->createDataArray($status['nom'], $status['idestat']);
            if (!$this->getStatus($status['idestat'])) {
                $this->createStatusWithId($statusData);
                echo Util::recordActionLog('status', $status['idestat'], 'create');
            } else {
                $this->updateStatus($statusData);
                echo Util::recordActionLog('status', $status['idestat'], 'update');
            }
        }
    }

}
