<?php

class BillingController extends Controller {

    //returns billing info by id (parameter) in an associative array
    function getBilling($id) {
        $billing = new Billing();
        return $billing->getBillingById($id);
    }

    //returns all table records in an associative array 
    function getAllBillings() {
        $billing = new Billing();
        return $billing->getAllBillings();
    }

    //inserts new record on database
    function createBilling(array $billingInfo) {
        $billing = new Billing();
        $billing->setDescription($billingInfo['description']);
        return $billing->createBilling();
    }

    //inserts new record on database
    function createBillingWithId(array $billingInfo) {
        $billing = new Billing();
        $billing->setId($billingInfo['id']);
        $billing->setDescription($billingInfo['description']);
        return $billing->createBillingWithId();
    }

    //deletes database record by id (parameter)
    function deleteBilling($id) {
        $billing = new Billing();
        $billing->deleteBillingById($id);
    }

    //updates record info
    function updateBilling(array $billingInfo) {
        $billing = new Billing();
        $billing->setId($billingInfo['id']);
        $billing->setDescription($billingInfo['description']);
        $billing->editBilling();
    }

    /*
     * Creates data array
     * with    id -> update
     * without id -> insert
     */

    function createDataArray($description, $id = null) {
        return array(
            'id' => $id,
            'description' => $description
        );
    }

    function migrateBilling() {
        /*
        $billingToMigrate = new TableToMigrate('facturacio', 'ALT');
        $billingToMigrateArray = $billingToMigrate->getMigrationData();
         */
                
        foreach ($this->getFullMigrationData('facturacio') as $billing) {
            $billingData = $this->createDataArray($billing['descripcio'], $billing['id']);
            if (!$this->getBilling($billing['id'])) {
                $this->createBillingWithId($billingData);
                echo Util::recordActionLog('billing', $billing['id'], 'create');
            } else {
                $this->updateBilling($billingData);
                echo Util::recordActionLog('billing', $billing['id'], 'update');
            }
        }
        
    }

}
