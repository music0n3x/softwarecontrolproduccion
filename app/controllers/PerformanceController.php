<?php

class PerformanceController extends Controller {

    //returns performance info by id (parameter) in an associative array
    function getPerformance($id) {
        $performance = new Performance();
        return $performance->getPerformanceById($id);
    }

    //returns all table records in an associative array 
    function getAllPerformances() {
        $performance = new Performance();
        return $performance->getAllPerformances();
    }

    //inserts new record on database
    function createPerformance(array $performanceInfo) {
        $performance = new Performance();
        $performance->setDescription($performanceInfo['description']);
        return $performance->createPerformance();
    }

    //inserts new record on database
    function createPerformanceWithId(array $performanceInfo) {
        $performance = new Performance();
        $performance->setId($performanceInfo['id']);
        $performance->setDescription($performanceInfo['description']);
        return $performance->createPerformanceWithId();
    }

    //deletes database record by id (parameter)
    function deletePerformance($id) {
        $performance = new Performance();
        $performance->deletePerformanceById($id);
    }

    //updates record info
    function updatePerformance(array $performanceInfo) {
        $performance = new Performance();
        $performance->setId($performanceInfo['id']);
        $performance->setDescription($performanceInfo['description']);
        $performance->editPerformance();
    }

    /*
     * Creates data array
     * with    id -> update
     * without id -> insert
     */

    function createDataArray($description, $id = null) {
        return array(
            'id' => $id,
            'description' => $description
        );
    }

    function migratePerformance() {
        foreach ($this->getFullMigrationData('tipo') as $performance) {
            $performanceData = $this->createDataArray($performance['nom'], $performance['idtipo']);
            if (!$this->getPerformance($performance['idtipo'])) {
                $this->createPerformanceWithId($performanceData);
                echo Util::recordActionLog('performance', $performance['idtipo'], 'create');
            } else {
                $this->updatePerformance($performanceData);
                echo Util::recordActionLog('performance', $performance['idtipo'], 'update');
            }
        }
    }

}
