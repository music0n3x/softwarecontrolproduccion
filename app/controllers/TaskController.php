<?php

class TaskController extends Controller {

    //returns record info by id (parameter) in an associative array
    function getTask($id) {
        $task = new Task();
        return $task->getTaskById($id);
    }

    function getTaskName($id) {
        $task = $this->getTask($id);
        return $task['title'];
    }
 
    function getProjectTasks($projectId) {
        $task = new Task();
        return $task->getAllByAlt('project_id', $projectId);
    }

    //returns all table records in an associative array 
    function getAllTasks() {
        $task = new Task();
        return $task->getAllTasks();
    }

    //inserts new record on database
    function createTask(array $taskInfo) {
        $task = new Task();
        $task->setInfo(Util::formatString($taskInfo['info']));
        $task->setTitle(Util::formatString($taskInfo['title']));
        $task->setHours($taskInfo['hours']);
        $task->setCreatedDate($taskInfo['createdDate']);
        $task->setStartDate($taskInfo['startDate']);
        $task->setFinishDate($taskInfo['finishDate']);
        $task->setProjectId($taskInfo['projectId']);
        $task->setStatusId($taskInfo['statusId']);
        $task->setPerformanceId($taskInfo['performanceId']);
        $task->setBillingId($taskInfo['billingId']);
        return $task->createTask();
    }

    //inserts new record on database with id
    function createTaskWithId(array $taskInfo) {
        $task = new Task();
        $task->setId($taskInfo['id']);
        $task->setInfo(Util::formatString($taskInfo['info']));
        $task->setTitle(Util::formatString($taskInfo['title']));
        $task->setHours($taskInfo['hours']);
        $task->setCreatedDate($taskInfo['createdDate']);
        $task->setStartDate($taskInfo['startDate']);
        $task->setFinishDate($taskInfo['finishDate']);
        $task->setProjectId($taskInfo['projectId']);
        $task->setStatusId($taskInfo['statusId']);
        $task->setPerformanceId($taskInfo['performanceId']);
        $task->setBillingId($taskInfo['billingId']);
        return $task->createTaskWithId();
    }

    //deletes database record by id (parameter)
    function deleteTask($id) {
        $task = new Task();
        $task->deleteTaskById($id);
    }

    //updates record info
    function updateTask(array $taskInfo) {
        $task = new Task();
        $task->setId($taskInfo['id']);
        $task->setInfo(Util::formatString($taskInfo['info']));
        $task->setTitle(Util::formatString($taskInfo['title']));
        $task->setHours($taskInfo['hours']);
        $task->setCreatedDate($taskInfo['createdDate']);
        $task->setStartDate($taskInfo['startDate']);
        $task->setFinishDate($taskInfo['finishDate']);
        $task->setProjectId($taskInfo['projectId']);
        $task->setStatusId($taskInfo['statusId']);
        $task->setPerformanceId($taskInfo['performanceId']);
        $task->setBillingId($taskInfo['billingId']);
        $task->editTask();
    }

    //returns every single project of a client in an associative array
    function getProjectTasksMatchingTaskId($projectId, array $matchingTasksId) {
        $task = new Task();
        $projectTasks = array();
        foreach ($task->getAllTasks() as $task) {
            if ($task['project_id'] == $projectId && Util::checkMatchFromValueToArray($task['id'], $matchingTasksId)) {
                array_push($projectTasks, $task);
            }
        }
        return $projectTasks;
    }

    public static function calculateHoursRealized($taskId) {
        $technicianTaskController = new TechnicianTaskController();
        $arrayTechnicianTasks = $technicianTaskController->getAllTaskTechnicianTasks($taskId);
        $hours = 0;
        foreach ($arrayTechnicianTasks as $technicianTask) {
            $hours += $technicianTask['minutes'];
        }
        return Util::convertToHoursMins($hours);
    }
    
    public static function calculateHoursRealizedByTechnician($taskId, $technicianId) {
        $technicianTaskController = new TechnicianTaskController();
        $arrayTechnicianTasks = $technicianTaskController->getTechnicianTaskByIdAndTaskId($taskId, $technicianId);
        $hours = 0;
        foreach ($arrayTechnicianTasks as $technicianTask) {
            $hours += $technicianTask['minutes'];
        }
        return Util::convertToHoursMins($hours);
    }
    
    public static function calculateHoursRealizedDateRange($taskId, $startDate, $endDate) {
        $technicianTaskController = new TechnicianTaskController();
        $arrayTechnicianTasks = $technicianTaskController->getTechnicianTasksOnDateRangeWhereTaskId($startDate, $endDate, $taskId);
        $hours = 0;
        foreach ($arrayTechnicianTasks as $technicianTask) {
            $hours += $technicianTask['minutes'];
        }
        return Util::convertToHoursMins($hours);
    }
    
    public static function calculateHoursEstimation($taskId) {
        $taskController = new TaskController();
        $task = $taskController->getTask($taskId);
        return $task['hours'];
    }

    function getWholeTasksByProjectId($projectId) {
        $task = new Task();
        return $task->getAllByAlt('project_id', $projectId);
    }

    function getTaskByIdAndProjectId($projectId, $taskId) {
        $task = new Task();
        $searchedTask = $task->getAllByAlt2Clauses('project_id', $projectId, 'id', $taskId);
        if (!empty($searchedTask)) {
            return $searchedTask[0];
        } else {
            return NULL;
        }
    }

    function getProjectTasksOnDateRange($startDate, $finishDate) {
        $task = new Task();
        return $task->getAllInDateRange($startDate, $finishDate, 'start_date');
    }

    function getTaskHours($taskId) {
        $technicianTaskController = new TechnicianTask();
        $technicianTasks = $technicianTaskController->getAllByAlt('task_id', $taskId);
        $minutes = 0;
        foreach ($technicianTasks as $technicianTask) {
            $minutes += $technicianTask['minutes'];
        }
        return $minutes;
    }

    /*
     * Creates data array
     * with    id -> update
     * without id -> insert
     */

    function createDataArray($info, $title, $hours, $createdDate, $startDate, $finishDate, $projectId, $statusId, $performanceId, $billingId, $id = null) {
        return array(
            'id' => $id,
            'info' => $info,
            'title' => $title,
            'hours' => $hours,
            'createdDate' => $createdDate,
            'startDate' => $startDate,
            'finishDate' => $finishDate,
            'projectId' => $projectId,
            'statusId' => $statusId,
            'performanceId' => $performanceId,
            'billingId' => $billingId
        );
    }

    function migrateTask() {
        foreach ($this->getFullMigrationData('faenes') as $task) {
            $taskData = $this->createDataArray($task['descripcio'], $task['titol'], $task['hores'], $task['data_inici'], $task['data_entrega'], $task['data_fin'], $task['idprojecte'], $task['idestat'], $task['idtipo'], $task['idfacturacio'], $task['idfaena']);
            if (!$this->getTask($task['idfaena'])) {
                $this->createTaskWithId($taskData);
                echo Util::recordActionLog('task', $task['idfaena'], 'create');
            } else {
                $this->updateTask($taskData);
                echo Util::recordActionLog('task', $task['idfaena'], 'update');
            }
        }
    }

}
