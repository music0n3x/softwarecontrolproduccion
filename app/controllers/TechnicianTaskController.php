<?php

class TechnicianTaskController extends Controller {

    //returns record info by id (parameter) in an associative array
    function getTechnicianTask($id) {
        $technicianTask = new TechnicianTask();
        return $technicianTask->getTechnicianTaskById($id);
    }

    public static function getProjectByTechnicianTaskId($technicianTaskId) {
        $technicianTaskController = new TechnicianTaskController();
        $taskController = new TaskController();
        $projectController = new ProjectController();
        $technicianTask = $technicianTaskController->getTechnicianTask($technicianTaskId);
        $task = $taskController->getTask($technicianTask['task_id']);
        $project = $projectController->getProjectInfo($task['project_id']);
        return $project;
    }

    function getIdsInDateRange($startDate, $finishDate) {
        $technicianTask = new TechnicianTask();
        return $technicianTask->getAllInDateRange($startDate, $finishDate, 'start_date', 'id');
    }

    function getTaskIdsInDateRange($startDate, $finishDate) {
        $technicianTask = new TechnicianTask();
        $tasksIdsArray = $technicianTask->getAllInDateRange($startDate, $finishDate, 'start_date', 'task_id');
        $finalArray = array();
        foreach ($tasksIdsArray as $taskId) {
            array_push($finalArray, $taskId['task_id']);
        }
        return $finalArray;
    }
    
    function getTechnicianTasksOnDateRangeWhereTaskId($startDate, $finishDate, $taskId){
        $technicianTask = new TechnicianTask();
        return $technicianTask->getAllInDateRangeWhere($startDate, $finishDate, 'start_date', 'task_id', $taskId);
    }
            
    function getTechnicianTaskByIdAndTaskId($taskId, $technicianId) {
        $technicianTask = new TechnicianTask();
        return $technicianTask->getAllByAlt2Clauses('task_id', $taskId, 'technician_id', $technicianId);
        if (!empty($searchedTechnicianTask)) {
            return $searchedTechnicianTask[0];
        }else {
            return NULL;
        }
    }

    //returns all table records in an associative array 
    function getAllTechnicianTasks() {
        $technicianTask = new TechnicianTask();
        return $technicianTask->getAllTechnicianTasks();
    }

    function getTechnicianTaskIdByTaskId($taskId) {
        $technicianTask = new TechnicianTask();
        return $technicianTask->getTechnicianTaskIdByTaskId($taskId);
    }

    public function getAllTechnicianTasksByTaskId($taskId) {
        $technicianTask = new TechnicianTask();
        return $technicianTask->getAllTechnicianTasksByTaskId($taskId);
    }

    function getAllDistinctTaskIdOfTechnician($technicianId) {
        $technicianTask = new TechnicianTask();
        $arrayTasksId = $technicianTask->getAllDistinctAlt('task_id', 'technician_id', $technicianId, 'start_date');
        $filteredArray = array ();
        foreach ($arrayTasksId as $taskId){
            array_push($filteredArray, $taskId['task_id']);
        }
        return $filteredArray;
    }

    function getTechnicianTasksBy2Clauses($column1, $value1, $column2, $value2) {
        $technicianTask = new TechnicianTask();
        return $technicianTask->getAllByAlt2Clauses($column1, $value1, $column2, $value2);
    }

    function getAllTaskTechnicianTasks($id) {
        $technicianTask = new TechnicianTask();
        return $technicianTask->getAllByAlt('task_id', $id);
    }

    function getAllTechnicianTechnicianTasks($technicianId) {
        $technicianTask = new TechnicianTask();
        return $technicianTask->getAllByAlt('technician_id', $technicianId);
    }

    //inserts new record on database
    function createTechnicianTask(array $technicianTaskInfo) {
        $technicianTask = new TechnicianTask();
        $technicianTask->setStartDate($technicianTaskInfo['startDate']);
        $technicianTask->setMinutes($technicianTaskInfo['minutes']);
        $technicianTask->setDescription(Util::formatString($technicianTaskInfo['description']));
        $technicianTask->setTaskId($technicianTaskInfo['taskId']);
        $technicianTask->setTechnicianId($technicianTaskInfo['technicianId']);
        return $technicianTask->createTechnicianTask();
    }

    //inserts new record on database with id
    function createTechnicianTaskWithId(array $technicianTaskInfo) {
        $technicianTask = new TechnicianTask();
        $technicianTask->setId($technicianTaskInfo['id']);
        $technicianTask->setStartDate($technicianTaskInfo['startDate']);
        $technicianTask->setMinutes($technicianTaskInfo['minutes']);
        $technicianTask->setDescription(Util::formatString($technicianTaskInfo['description']));
        $technicianTask->setTaskId($technicianTaskInfo['taskId']);
        $technicianTask->setTechnicianId($technicianTaskInfo['technicianId']);
        return $technicianTask->createTechnicianTask();
    }

    //deletes database record by id (parameter)
    function deleteTechnicianTask($id) {
        $technicianTask = new TechnicianTask();
        $technicianTask->deleteTechnicianTaskById($id);
    }

    //updates record info
    function updateTechnicianTask(array $technicianTaskInfo) {
        $technicianTask = new TechnicianTask();
        $technicianTask->setId($technicianTaskInfo['id']);
        $technicianTask->setStartDate($technicianTaskInfo['startDate']);
        $technicianTask->setMinutes($technicianTaskInfo['minutes']);
        $technicianTask->setDescription(Util::formatString($technicianTaskInfo['description']));
        $technicianTask->setTaskId($technicianTaskInfo['taskId']);
        $technicianTask->setTechnicianId($technicianTaskInfo['technicianId']);
        $technicianTask->editTechnicianTask();
    }

    /*
     * Creates data array
     * with    id -> update
     * without id -> insert
     */

    function createDataArray($startDate, $minutes, $description, $taskId, $technicianId, $id = null) {
        return array(
            'id' => $id,
            'startDate' => $startDate,
            'minutes' => $minutes,
            'description' => $description,
            'taskId' => $taskId,
            'technicianId' => $technicianId
        );
    }

    function migrateTechnicianTask() {
        foreach ($this->getFullMigrationData('hores') as $technicianTask) {
            $technicianTaskData = $this->createDataArray($technicianTask['fecha_insertat'], $technicianTask['minuts_invertits'], $technicianTask['descripcio'], $technicianTask['idfaena'], $technicianTask['idtecnic'], $technicianTask['idhores']);
            if (!$this->getTechnicianTask($technicianTask['idhores'])) {
                $this->createTechnicianTaskWithId($technicianTaskData);
                echo Util::recordActionLog('technicianTask', $technicianTask['idhores'], 'create');
            } else {
                $this->updateTechnicianTask($technicianTaskData);
                echo Util::recordActionLog('technicianTask', $technicianTask['idhores'], 'update');
            }
        }
    }

}
