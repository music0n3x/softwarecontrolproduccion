<?php

class Database extends PDO {

    public function __construct($dbSelect = FALSE) {
        if (!($dbSelect)) {
            parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS,
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
        } else {
            parent::__construct(DB_TYPE_2 . ':host=' . DB_HOST_2 . ';dbname=' . DB_NAME_2, DB_USER_2, DB_PASS_2,
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
        }
    }

}
