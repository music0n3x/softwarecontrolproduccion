<?php

class Controller {

    public function __construct() {
        foreach (glob($_SERVER['DOCUMENT_ROOT'] . "/app/models/*.php") as $file) {
            require_once $file;
        }
    }

    public function debugVar($data, $dump = false) {
        echo "<pre class='debug_var'>";
        if ($dump) {
            var_dump($data);
        } else {
            print_r($data);
        }
        echo "</pre>";
    }

    function getFullMigrationData($tableName) {
        $migrationData = new TableToMigrate($tableName, 'ALT');
        return $migrationData->getAll();
    }

}
