<?php

class Util {

    public static function addslashesExtended(&$mixed) {
        if (is_array($mixed) || is_object($mixed)) {
            array_walk($mixed, 'Util::addslashesExtended');
        } elseif (is_string($mixed)) {
            $mixed = addslashes($mixed);
        }
    }

    public static function striplashesExtended(&$mixed) {
        if (is_array($mixed) || is_object($mixed)) {
            array_walk($mixed, 'Util::striplashesExtended');
        } elseif (is_string($mixed)) {
            $mixed = stripslashes($mixed);
        }
    }

    public static function htmlspecialcharsExtended(&$mixed) {
        if (is_array($mixed) || is_object($mixed)) {
            array_walk($mixed, 'Util::htmlspecialcharsExtended');
        } elseif (is_string($mixed)) {
            $mixed = htmlspecialchars($mixed, ENT_QUOTES, 'UTF-8');
        }
    }

    public static function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                Util::utf8_encode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                Util::utf8_encode_deep($input->$var);
            }
        }
    }

    public static function utf8_decode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_decode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                Util::utf8_decode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                Util::utf8_decode_deep($input->$var);
            }
        }
    }

    public static function set_option_selected(&$list, &$object, $fieldObject, $fieldList = 'id') { //by default fieldList is id
        foreach ($list as &$item) {
            $item['selected'] = '';
            if ($item[$fieldList] == $object[$fieldObject]) {
                $item['selected'] = 'selected="selected"';
            }
        }
    }

    public static function set_selected(&$list, $key, $value) {
        foreach ($list as &$item) {
            $item['selected'] = '';
            if ($item[$key] == $value) {
                $item['selected'] = 'selected';
            }
        }
    }

    public static function set_checkbox_checked(&$object, $fieldObject, $key = "checked") {
        if ($object[$fieldObject]) {
            $object[$key] = 'checked="checked"';
        } else {
            $object[$key] = "";
        }
    }

    public static function set_checked(&$list, $key) {
        foreach ($list as &$item) {
            $item['checked'] = '';
            if ($item[$key]) {
                $item['checked'] = 'checked="checked"';
            }
        }
    }

    public static function generate_array_language($field, $lang, $table, $ident) {
        $language = array();
        $language["field"] = $field;
        $language["lang"] = $lang;
        $language["table"] = $table;
        $language["ident"] = $ident;
        return $language;
    }

    public static function generate_random_string($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function checkbox_to_boolean($value) {
        return (isset($value)) ? 1 : 0;
    }

    public static function delete_duplicates_in_array($list) {
        return array_map("unserialize", array_unique(array_map("serialize", $list)));
    }

    public static function delete_duplicates_in_array_by_key($array, $key) {
        $temp_array = array();
        foreach ($array as &$v) {
            if (!isset($temp_array[$v[$key]]))
                $temp_array[$v[$key]] = & $v;
        }
        $array = array_values($temp_array);
        return $array;
    }

    public static function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && Util::in_array_r($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }

    public static function generate_string_by_commas($array, $index) {
        $stringList = "";

        foreach ($array as $value) {
            $stringList .= $value . ",";
        }

        $stringList .= "-1";

        return $stringList;
    }

    public static function get_values_from_array_by_index($array, $index) {
        $arrayAux = array();

        foreach ($array as $value) {
            $arrayAux[] = $value[$index];
        }
        return Util::generate_string_by_commas($arrayAux, $index);
    }

    public static function debug_var($data, $dump = false) {
        echo "<pre class='debug_var'>";
        if ($dump) {
            var_dump($data);
        } else {
            print_r($data);
        }
        echo "</pre>";
    }

    public static function datetimeToSpanishFormat($source) {
        $date = new DateTime($source);
        return $date->format("d/m/Y h:m:s");
    }

    public static function datetimeToMysqlFormat($source) {
        $date = new DateTime($source);
        return $date->format("Y-m-d h:m:s");
    }

    public static function dateToSpanishFormat($source) {
        $date = new DateTime($source);
        $newDate = $date->format("d/m/Y");
        if ($newDate == "30/11/-0001") {
            return "- - - - - - - -";
        } else {
            return $newDate;
        }
    }

    public static function dateSpanishToMysqlFormat($source) {
        $date = DateTime::createFromFormat('d/m/Y', $source);
        return $date->format("Y-m-d");
    }

    public static function datetimeSpanishToMysqlFormat($source) {
        return date('Y-m-d H:i:s', strtotime($source));
    }

    public static function dateToMysqlFormat($source) {
        $date = new DateTime($source);
        return $date->format("Y-m-d");
    }

    public static function set_date_to_spanish_format(&$list, $key) {
        foreach ($list as &$item) {
            $item[$key] = Util::dateToSpanishFormat($item[$key]);
        }
    }

    public static function set_date_time_to_spanish_format(&$list, $key) {
        foreach ($list as &$item) {
            $item[$key] = Util::dateToMysqlFormat($item[$key]);
        }
    }

    public static function urlencode_errorCode($errorCode) {
        return ADMIN_ERROR_URL . "&ec=" . urlencode($errorCode);
    }

    public static function generate_timthumb_url_item(&$item, $key, $urlTimThumb, $params) {
        $item['timthumb'] = "";
        if ($item[$key]) {
            $item['timthumb'] = $urlTimThumb . $item[$key] . $params;
        }
    }

    public static function generate_timthumb_url(&$list, $key, $params = TIMTHUMB_PARAMS, $urlTimThumb = URL_TIMTHUMB) {
        foreach ($list as &$item) {
            $item['timthumb'] = "";
            if (file_exists(DIR_UPLOAD_ICONS . $item[$key])) {
                $item['timthumb'] = $urlTimThumb . str_replace(' ', '%20', $item[$key]) . $params;
            }
        }
    }

    public static function generate_image_url(&$list, $key) {
        foreach ($list as &$item) {
            $item['image_url'] = "";
            if (file_exists(DIR_UPLOAD_ICONS . $item[$key])) {
                $item['image_url'] = UPLOAD_URL . str_replace(' ', '%20', $item[$key]);
            }
        }
    }

    public static function generate_array_to_export_csv($list) {
        $dataArrayExport = array();

        $i = 0;
        foreach ($list as $listItem) {
            $j = 0;
            foreach ($listItem as $item => $value) {
                $dataArrayExport[$i][$j] = $value;
                $j++;
            }
            $i++;
        }
        return $dataArrayExport;
    }

    public static function upload_file_csv($file, $ext) {

        $result = false;

        if (!empty($file)) {
            $upload = new Upload($file);
            $upload->file_new_name_body = $upload->file_src_name_body;
            $upload->file_new_name_ext = $ext;
            if ($upload->uploaded) {
                $upload->Process(DIR_UPLOAD_CSV);
                if ($upload->processed) {
                    $result = $upload->file_src_name;
                } else {
                    $result = $upload->error;
                }
            }
            //echo $upload->log;
            $upload->Clean();

            return $result;
        }

        return $result;
    }

    public static function mapped_array_langs_empty(&$array, $langs, $field = "name") {
        $auxLangName = array();
        foreach ($langs as $item) {
            $auxLangName[$item['abbrev']] = "";
        }

        $array[$field] = $auxLangName;
    }

    public static function formated_string_number_to_db($val) {
        $val = str_replace(",", ".", $val);
        $val = preg_replace('/\.(?=.*\.)/', '', $val);
        return floatval($val);
    }

    public static function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }

    public static function exploded_post_by_name($post, $name, $delimiter) {
        $string = "";

        foreach ($post as $item => $value) {
            $exploded = explode($delimiter, $item);
            if ($exploded[0] == $name) {
                $string .= $exploded[1] . ",";
            }
        }

        $string = substr($string, 0, -1);

        return $string;
    }

    public static function array_push_assoc(&$array, $key, $value) {
        $array[$key] = $value;
        return $array;
    }

    public static function compareDeepValue($val1, $val2) {
        return strcmp($val1['id'], $val2['id']);
    }

    public static function check_date_in_range($start_date, $end_date, $date_from_user) {
        $start_ts = $end_ts = "";
        if ($start_date) {
            $start_ts = strtotime(Util::dateSpanishToMysqlFormat($start_date));
        }
        if ($end_date) {
            $end_ts = strtotime(Util::dateSpanishToMysqlFormat($end_date));
        }
        $user_ts = strtotime(Util::dateSpanishToMysqlFormat($date_from_user));

        if ($start_ts && $end_ts) {
            $isInRange = (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
        } elseif ($start_ts && !$end_ts) {
            $isInRange = (($user_ts >= $start_ts));
        } elseif (!$start_ts && $end_ts) {
            $isInRange = (($user_ts <= $end_ts));
        }

        return $isInRange;
    }

    /* -- popup -- */

    public static function isPopup() {
        if ((isset($_GET["popup"]) && $_GET["popup"] == 1) || (isset($_POST["popup"]) && $_POST["popup"] == 1)) {
            return true;
        }
        return false;
    }

    public static function closePopup() {
        echo "<script>window.close();</script>";
    }

    public static function mysql_escape_mimic($inp) {
        if (is_array($inp))
            return array_map(__METHOD__, $inp);

        if (!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }

        return $inp;
    }

    public static function formatString($stringToFormat) {
        return html_entity_decode(self::mysql_escape_mimic($stringToFormat));
    }

    public static function recordActionLog($tableName, $id, $action) {
        if ($action == 'create') {
            return "$tableName created with id: " . $id . '<br>';
        } else {
            return "updated $tableName with id: " . $id . '<br>';
        }
    }

    public static function errorMsg($mysqlException) {
        echo 'Error code: ' . $mysqlException->getCode() . '<br>';
        echo 'Error message: ' . $mysqlException->getMessage() . '<br>';
        echo 'Origination file: ' . $mysqlException->getFile() . '<br>';
        echo 'Crash on line: ' . $mysqlException->getLine();
    }

    public static function redirect($url, $statusCode = 303) {
        header('Location: ' . $url, true, $statusCode);
        die();
    }

    public static function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    public static function calculateLang() {
        $lang = LANG;
        if ($lang != 'lang=es') {
            return $lang;
        }
    }

    public static function generateRoute($sec, $act = null) {
        if ($act != NULL) {
            return "index.php?sec=$sec&act=$act&" . LANG;
        } else {
            return "index.php?sec=$sec&" . LANG;
        }
    }

    public static function generateRouteChangeLang($sec, $act = null) {
        if (LANG == "lang=es") {
            $lang = "en";
        } else {
            $lang = "es";
        }
        if ($act != NULL) {
            return "index.php?sec=$sec&act=$act&lang=" . $lang;
        } else {
            return "index.php?sec=$sec&lang=" . $lang;
        }
    }

    public static function generateLabelAndRoute($label, $href) {
        return array(
            "label" => $label,
            "href" => $href
        );
    }

    public static function generateBreadCrumb($first, $second = null, $third = null, $fourth = null) {
        $breadcrumb = array();
        if (isset($first)) {
            array_push($breadcrumb, $first);
        }
        if (isset($second)) {
            array_push($breadcrumb, $second);
        }
        if (isset($third)) {
            array_push($breadcrumb, $third);
        }
        if (isset($fourth)) {
            array_push($breadcrumb, $fourth);
        }
        return $breadcrumb;
    }

    public static function checkIfExists($array, $key, $val) {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val)
                return true;
        return false;
    }

    public static function checkMatchFromValueToArray($value, $array) {
        foreach ($array as $arrayValue) {
            if ($value == $arrayValue) {
                return TRUE;
            }
        }
        return FALSE;
    }

}
