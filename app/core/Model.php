<?php

class Model {

    function __construct($table, $dbInfo = FALSE) {
        $this->table = (string) $table;
        if (!$dbInfo) {
            $this->db = new Database('ALT');
        } else {
            $this->db = new Database();
        }
        //$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }

    public function getAll() {
        $sth = $this->db->prepare("SELECT * FROM $this->table");
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getAllAlphabetically($fieldToOrderBy) {
        $sth = $this->db->prepare("SELECT * FROM $this->table ORDER BY $fieldToOrderBy ASC");
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllLimit($limit) {
        $sth = $this->db->prepare("SELECT * FROM $this->table LIMIT $limit");
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllOrderBy($column, $order = "ASC") {
        $sth = $this->db->prepare("SELECT * FROM $this->table ORDER BY $column $order");
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById($id) {
        $sth = $this->db->prepare("SELECT * FROM $this->table WHERE id = :id");
        $sth->execute(array(':id' => $id));
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    public function getBy($column, $value) {
        $sth = $this->db->prepare("SELECT * FROM $this->table WHERE $column='$value'");
        $sth->execute(array(':' . $column => $value));
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    public function getAllBy($column, $value) {
        $sql = "SELECT * FROM $this->table WHERE $column='$value'";
        $sth = $this->db->prepare($sql);
        $sth->execute(array(':' . $column => $value));
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllByAndOrder($column, $value, $columnOrder, $order = "ASC") {
        $sql = "SELECT * FROM $this->table WHERE $column='$value' ORDER BY $columnOrder $order";
        $sth = $this->db->prepare($sql);
        $sth->execute(array(':' . $column => $value));
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function deleteById($id) {
        $query = $this->db->query("DELETE FROM $this->table WHERE id=$id");
        return $query;
    }

    public function deleteBy($column, $value) {
        $query = $this->db->query("DELETE FROM $this->table WHERE $column='$value'");
        return $query;
    }

    public function getFieldsListTable() {
        $sth = $this->db->prepare("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$this->table'");
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getFieldsByColumnString($column, $search_term) {
        $sth = $this->db->prepare("SELECT * FROM $this->table WHERE $column LIKE :search");
        $sth->execute(array(':search' => '%' . $search_term . '%'));
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getFieldsByColumnStringConfig($column, $search_term) {
        $sth = $this->db->prepare("SELECT * FROM $this->table WHERE $column LIKE :search");
        $sth->execute(array(':search' => $search_term));
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getFieldsByColumnIn($column, $in) {
        $sth = $this->db->prepare("SELECT * FROM $this->table WHERE $column IN ($in)");
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function truncateTable() {
        $query = $this->db->query("TRUNCATE TABLE $this->table");
        return $query;
    }

    public function getAllDistinct($field) {
        $sth = $this->db->prepare("SELECT DISTINCT($field) FROM $this->table");
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getMaxValueByColumn($column) {
        $sth = $this->db->prepare("SELECT MAX($column) AS idMax FROM $this->table");
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    public function queryGeneratorByData($data, $id) {
        $sql = "UPDATE $this->table SET";
        foreach ($data as $item => $value) {
            if ($value !== "") {
                $sql .= " $item = '$value',";
            }
        }
        $sql = substr($sql, 0, -1);
        $sql .= " WHERE id = $id";
        //Util::debug_var($sql);

        $query = $this->db->query($sql);
        return $query;
    }

    public function getListColumnsTable() {
        $columsTable = $this->getFieldsListTable();

        $listColumns = "";
        foreach ($columsTable as $colum) {
            if ($colum["COLUMN_NAME"] != "id") {
                $listColumns .= $colum["COLUMN_NAME"] . ",";
            }
        }
        $listColumns = substr($listColumns, 0, -1);

        return $listColumns;
    }

    public function getAllByAlt($column, $value) {
        $sql = "SELECT * FROM $this->table WHERE $column='$value'";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllByAltOrdered($column, $value, $columnOrder, $order = "DESC") {
        $sql = "SELECT * FROM $this->table WHERE $column='$value' ORDER BY $columnOrder $order";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllDistinctAlt($selectedField1, $secondFieldCol, $secondFieldValue, $columnOrder, $order = "DESC") {
        $sth = $this->db->prepare("SELECT DISTINCT($selectedField1) FROM $this->table WHERE $secondFieldCol='$secondFieldValue' ORDER BY $columnOrder $order");
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllByAlt2Clauses($column1, $value1, $column2, $value2) {
        $sql = "SELECT * FROM $this->table WHERE $column1='$value1' AND $column2='$value2'";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllInDateRange($startDate, $finishDate, $dateField, $selectField) {
        $sql = "SELECT $selectField FROM $this->table WHERE $dateField BETWEEN '$startDate' and '$finishDate'";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllInDateRangeWhere($startDate, $finishDate, $dateField, $whereField, $whereValue) {
        $sql = "SELECT * FROM $this->table WHERE $dateField BETWEEN '$startDate' and '$finishDate' and $whereField='$whereValue'";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

}
