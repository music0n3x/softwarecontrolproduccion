<?php

class Languages extends Model {

    private $id;
    private $text;
    private $tabl;
    private $field;
    private $lang;
    private $ident;

    public function __construct($table="languages") {
        parent::__construct($table);
    }

    public function getId() {
        return $this->id;
    }
    
    public function getText(){
        return $this->text;
    }
    
    public function getTable(){
        return $this->tabl;
    }
    
    public function getField(){
        return $this->field;
    }
    
    public function getLang(){
        return $this->lang;
    }
    
    public function getIdent(){
        return $this->ident;
    }

    public function setId($id) {
        $this->id = $id;
    }
    
    public function setText($text) {
        $this->text = $text;
    }
    
    public function setTable($table) {
        $this->tabl = $table;
    }
    
    public function setField($field) {
        $this->field = $field;
    }
    
    public function setLang($lang) {
        $this->lang = $lang;
    }
    
    public function setIdent($ident) {
        $this->ident = $ident;
    }

    public function listLanguages() {
        return $this->getAll();
    }

    public function listLanguagesId() {
        return $this->getById($this->id);
    }

    public function deleteLanguagesId() {
        return $this->deleteById($this->id);
    }
    
    public function searchStringInTable($search_term){
        $sth = $this->db->prepare("SELECT DISTINCT(ident) FROM languages WHERE languages.table = :table AND languages.text LIKE :search");
        $sth->execute(array(':table' => $this->tabl,':search' => '%'.$search_term.'%'));
        return $sth->fetchAll();
    }
    
    public function getItemsBySection(){
        $sth = $this->db->prepare("SELECT id FROM languages WHERE languages.table = :table AND languages.ident = :ident");
        $sth->execute(array(':table' => $this->tabl,':ident' => $this->ident));
        return $sth->fetchAll();
    }
    
    public function getItemByLang() {
        $sth = $this->db->prepare("SELECT id, text FROM languages WHERE languages.table = :table AND languages.field = :field AND languages.ident = :ident AND languages.lang = :lang");
        $sth->execute(array(':table' => $this->tabl,':field' => $this->field,':ident' => $this->ident,':lang' => $this->lang));
        return $sth->fetch();
    }

    public function updateLanguages() {
        if ($this->id) {
            $sql = "UPDATE languages SET text = '".addslashes($this->text)."' WHERE id=$this->id";
        } else {
            $sql = "INSERT INTO languages (languages.text,languages.table,languages.field,languages.lang,languages.ident)"
                    . " VALUES ('".addslashes($this->text)."','$this->tabl','$this->field','$this->lang',$this->ident)";
        }
        //echo $sql."<br/>";
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function insertLanguages() {
        $sql = "INSERT INTO languages (languages.text,languages.table,languages.field,languages.lang,languages.ident)"
                . " VALUES ('".addslashes($this->text)."','$this->tabl','$this->field','$this->lang',$this->ident)";
        $query = $this->db->query($sql);
        return $query;
    }
    
    public function deleteLanguages() {
        $query = $this->db->query("DELETE FROM languages WHERE id = $this->id");
        return $query;
    }
    
    function getTextLanguages($data) {
        $this->setField($data['field']);
        $this->setLang($data['lang']);
        $this->setTable($data['table']);
        $this->setIdent($data['ident']);
        return $this->getItemByLang();
    }

    function updateTextLanguages($data) {
        $this->setText($data['text']);
        $this->parentSetId($data['id']);
        return $this->updateLanguages();
    }

    function insertTextLanguages($data) {
        $this->setText($data['text']);
        $this->setField($data['field']);
        $this->setLang($data['lang']);
        $this->setTable($data['table']);
        $this->setIdent($data['ident']);
        return $this->insertLanguages();
    }
    
    function getLanguagesApp(){
        $sth = $this->db->prepare("SELECT * FROM languages_app WHERE languages_app.active = 1 ORDER BY languages_app.default DESC");
        $sth->execute();
        return $sth->fetchAll();
    }
    
    function getLanguagesAppId($id){
        $sth = $this->db->prepare("SELECT * FROM languages_app WHERE languages_app.id = :id");
        $sth->execute(array(':id' => $id));
        return $sth->fetch();
    }
    
    function getLanguagesAppByLang($lang){
        $sth = $this->db->prepare("SELECT * FROM languages_app WHERE languages_app.abbrev = :abbrev");
        $sth->execute(array(':abbrev' => $lang));
        return $sth->fetch();
    }
    
    function updateMultiLanguagesField($object,$data,$field,$table){
        
        Util::debug_var($data);
        
        $object->setIdent($data['id']);
        $object->setTable($table);

        foreach ($data[$field] as $item){
            $object->setLang($item['lang']);
            $object->setField($item['field']);
            $dataLanguages = $object->getItemByLang();
            $object->setText($item['text']);
            $object->parentSetId($dataLanguages["id"]);
            $updateTextLanguages = $object->updateLanguages();
        }
        
        return $updateTextLanguages;
    }
    
    function insertMultiLanguagesField($object,$data,$field,$table){
        $object->setTable($table);     
        
        foreach ($data[$field] as $item){
            $object->setField($item['field']);
            $object->setLang($item['lang']);
            $object->setText($item['text']);
            $insertLanguages = $object->insertLanguages();
        }
        
        return $insertLanguages;
    }

}
