<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");
include_once '../app.php';

try {
    $performanceController = new PerformanceController();
    $performanceController->migratePerformance();
} catch (PDOException $mysqlException) {
    Util::errorMsg($mysqlException);
}