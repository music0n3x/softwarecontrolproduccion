<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");
include_once '../app.php';

try {
    $projectController = new ProjectController();
    $projectController->migrateProject();
} catch (PDOException $mysqlException) {
    Util::errorMsg($mysqlException);
}