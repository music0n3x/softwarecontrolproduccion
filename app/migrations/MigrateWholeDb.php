<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");
include '../app.php';

try {
    echo '<h4>MIGRATING BILLING TABLE...</h4>';
    $billingController = new BillingController();
    $billingController->migrateBilling();

    echo '<h4>MIGRATING CLIENT TABLE...</h4>';
    $clientController = new ClientController();
    $clientController->clientMigration();

    echo '<h4>MIGRATING PERFORMANCE TABLE...</h4>';
    $performanceController = new PerformanceController();
    $performanceController->migratePerformance();

    echo '<h4>MIGRATING PROJECT TABLE...</h4>';
    $projectController = new ProjectController();
    $projectController->migrateProject();

    echo '<h4>MIGRATING STATUS TABLE...</h4>';
    $statusController = new StatusController();
    $statusController->migrateStatus();

    echo '<h4>MIGRATING TASK TABLE...</h4>';
    $taskController = new TaskController();
    $taskController->migrateTask();

    echo '<h4>MIGRATING TECHNICIAN TABLE...</h4>';
    $technicianController = new TechnicianController();
    $technicianController->migrateTechnician();

    echo '<h4>MIGRATING TECHNICIANTASK TABLE...</h4>';
    $technicianTaskController = new TechnicianTaskController();
    $technicianTaskController->migrateTechnicianTask();
} catch (PDOException $mysqlException) {
    Util::errorMsg($mysqlException);
}
