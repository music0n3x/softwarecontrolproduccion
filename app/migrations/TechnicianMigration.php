<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");
include_once '../app.php';

try {
    $technicianController = new TechnicianController();
    $technicianController->migrateTechnician();
} catch (PDOException $mysqlException) {
    Util::errorMsg($mysqlException);
}