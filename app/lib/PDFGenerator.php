<?php

/**
 * https://tcpdf.org/examples/
 */

class PDFGenerator {

    private $html;
    private $author;
    private $title;
    private $subject;
    private $keywords;
    private $font;
    private $fontSize;
    private $fileName;
    private $outputType;

    public function __construct($html) {
        $this->html = $html;
        $this->author = "QualiartAPP";
        $this->title = "QualiartAPP document";
        $this->subject = "QualiartAPP document";
        $this->keywords = "qualiartapp, system";
        $this->font = "dejavusans";
        $this->fontSize = "10";
        $this->fileName = "qualiart-doc" . date(time()) . ".pdf";
        $this->outputType = "D";
    }
    
    public function getHtml() {
        return $this->html;
    }

    public function getAuthor() {
        return $this->author;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getSubject() {
        return $this->subject;
    }

    public function getKeywords() {
        return $this->keywords;
    }

    public function getFont() {
        return $this->font;
    }

    public function getFontSize() {
        return $this->fontSize;
    }

    public function getFileName() {
        return $this->fileName;
    }

    public function setHtml($html) {
        $this->html = $html;
    }

    public function setAuthor($author) {
        $this->author = $author;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
    }

    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    public function setFont($font) {
        $this->font = $font;
    }

    public function setFontSize($fontSize) {
        $this->fontSize = $fontSize;
    }

    public function setFileName($fileName) {
        $this->fileName = $fileName;
    }
    
    public function getOutputType() {
        return $this->outputType;
    }

    public function setOutputType($outputType) {
        $this->outputType = $outputType;
    }

    public function generatePDF() {

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($this->author);
        $pdf->SetTitle($this->title);
        $pdf->SetSubject($this->subject);
        $pdf->SetKeywords($this->keywords);

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 006', PDF_HEADER_STRING);
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setPrintHeader(false);
        
        // set header and footer fonts
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 10, 10);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetCellPadding(0);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
//        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        /*if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }*/

        // ---------------------------------------------------------
        // set font
        $pdf->SetFont($this->font, '', $this->fontSize);

        // add a page
        $pdf->AddPage();

        // writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
        // writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
        // create some HTML content
        // output the HTML content
        $pdf->writeHTML($this->html, true, false, true, false, '');

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // reset pointer to the last page
        $pdf->lastPage();

        // ---------------------------------------------------------
        //Close and output PDF document
        $pdf->Output($this->fileName, $this->outputType);
    }

}
