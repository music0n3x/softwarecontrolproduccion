<?php

class StaticLang {
    
    private $fileXml;
    
    public function __construct($fileXml) {
        $xml = new SimpleXMLElement($fileXml, NULL, TRUE);
        $json_string = json_encode($xml);
        $this->fileXml = json_decode($json_string, TRUE);
        //Util::debug_var($this->fileXml);
    }
    
    public function getArrayXmlStaticLanguages(){
        return $this->fileXml;
    }
    
    public function getTagByName($name){
        if(!isset($this->fileXml[$name])){
            $tag = "!_".$name;
        } else {
            $tag = $this->fileXml[$name];
        }
        return $tag;
    }

    public function getTagArray($tagList) {
        $aux = array();
        foreach ($tagList as $tag) {
            $aux[$tag] = $this->getTagByName($tag);
        }
        return $aux;
    }
    
    public function formatingArrayListByTagLanguages(&$list,$key){
        foreach ($list as &$item) {
            $item[$key] = $this->getTagByName($item[$key]);
        }
    }
}
