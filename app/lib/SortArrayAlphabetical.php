<?php

class SortArrayAlphabetical {

    private $actual_order = 'asc';
    private $actual_field = null;

    public function compare_arrays($array1, $array2) {
        
        $field1 = strtolower($array1[$this->actual_field]);
        $field2 = strtolower($array2[$this->actual_field]);
        
        if ($field1 == $field2) {
            return 0;
        } elseif ($field1 > $field2) {
            return ($this->actual_order == 'asc' ? 1 : -1);
        } else {
            return ($this->actual_order == 'asc' ? -1 : 1);
        }
    }

    public function order_array(&$array) {

        usort($array, array($this, 'compare_arrays'));
    }

    public function __construct($field, $actual_order = 'asc') {
        $this->actual_field = $field;
        $this->actual_order = $actual_order;
    }

}
