<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");

include '../app.php';
$technicianTaskController = new TechnicianTaskController();

//create technicianTask
echo '<b>Creating</b> technicianTask...<br>';
$id = $technicianTaskController->createTechnicianTask($technicianTaskController->createDataArray(date("Y-m-d"), 150, 1, 2));
echo "TechnicianTask created with id: $id<br><br>";

//get technicianTask
echo '<b>Getting</b> created technicianTask info...<br> TechnicianTask info: ';
print_r($technicianTaskController->getTechnicianTask($id));
echo "<br><br>";

//update technicianTask
echo '<b>Updating</b> technicianTask info<br><br>';
$technicianTaskController->updateTechnicianTask($technicianTaskController->createDataArray(date("Y-m-d"), 150, 1, 2, $id));

//get technicianTask
echo '<b>Getting</b> updated technicianTask info...<br> TechnicianTask info: ';
print_r($technicianTaskController->getTechnicianTask($id));
echo "<br><br>";

//delete technicianTask
echo '<b>Deleting</b> technicianTask...<br><br>';
$technicianTaskController->deleteTechnicianTask($id);

//get all technicianTasks
echo '<b>Getting all</b> technicianTasks...<br>';
foreach ($technicianTaskController->getAllTechnicianTasks() as $technicianTaskInfo) {
    print_r($technicianTaskInfo);
    echo '<br>';
}