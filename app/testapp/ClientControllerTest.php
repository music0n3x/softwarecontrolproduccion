<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");

include '../app.php';
$clientController = new ClientController();

//create client
echo '<b>Creating</b> client...<br>';
$id = $clientController->createClient($clientController->createDataArray('Daniel', 'rodamientosDaniel.com tlf:634 07 26 67'));
echo "Client created with id: $id<br><br>";

//get client
echo '<b>Getting</b> created client info...<br> Client info: ';
print_r($clientController->getClientInfo($id));
echo "<br><br>";

//update client
echo '<b>Updating</b> client info<br><br>';
$clientController->updateClientData($clientController->createDataArray('Jose', 'joseInfo.com', $id));

//get client
echo '<b>Getting</b> updated client info...<br> Client info: ';
print_r($clientController->getClientInfo($id));
echo "<br><br>";

//delete client
echo '<b>Deleting</b> client...<br><br>';
$clientController->deleteClient($id);

//get all clients
echo '<b>Getting all</b> clients...<br>';
foreach ($clientController->getAllClients() as $clientInfo) {
    print_r($clientInfo);
    echo '<br>';
}

//create client with id
echo '<br><br><b>Creating</b> client with id 69...<br>';
$id2 = $clientController->createClientWithId($clientController->createDataArray('Daniel', 'rodamientosDaniel.com tlf:634 07 26 67', 69));
echo "Client created with id: $id2<br><br>";

//get client
echo '<b>Getting</b> created client info...<br> Client info: ';
print_r($clientController->getClientInfo($id2));
echo "<br><br>";