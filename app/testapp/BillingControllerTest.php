<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");

include '../app.php';
$billingController = new BillingController();

//create billing
echo '<b>Creating</b> billing...<br>';
$id = $billingController->createBilling($billingController->createDataArray('billingDescription'));
echo "Billing created with id: $id<br><br>";

//get billing
echo '<b>Getting</b> created billing info...<br> Billing info: ';
print_r($billingController->getBilling($id));
echo "<br><br>";

//update billing
echo '<b>Updating</b> billing info<br><br>';
$billingController->updateBilling($billingController->createDataArray('updatedDescription', $id));

//get billing
echo '<b>Getting</b> created billing info...<br> Billing info: ';
print_r($billingController->getBilling($id));
echo "<br><br>";

//delete billing
echo '<b>Deleting</b> billing...<br><br>';
$billingController->deleteBilling($id);

//get all billings
echo '<b>Getting all</b> billings...<br>';
foreach ($billingController->getAllBillings() as $billingInfo) {
print_r($billingInfo);
echo '<br>';
}

//create billing with id
echo '<br><br><b>Creating</b> billing with id 69...<br>';
$id2 = $billingController->createBillingWithId($billingController->createDataArray('Esta es la descripción', 69));
echo "Billing created with id: $id2<br><br>";

//get billing
echo '<b>Getting</b> created billing info...<br> Client info: ';
print_r($billingController->getBilling($id2));
echo "<br><br>";