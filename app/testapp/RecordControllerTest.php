<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");

include '../app.php';
$recordController = new RecordController();

//create record
echo '<b>Creating</b> record...<br>';
$id = $recordController->createRecord($recordController->createDataArray(date("Y-m-d"), date("Y-m-d"), 2));
echo "Record created with id: $id<br><br>";

//get record
echo '<b>Getting</b> created record info...<br> Record info: ';
print_r($recordController->getRecord($id));
echo "<br><br>";

//update record
echo '<b>Updating</b> record info<br><br>';
$recordController->updateRecord($recordController->createDataArray(date("Y-m-d"), date("Y-m-d"), 2, $id));

//get record
echo '<b>Getting</b> updated record info...<br> Record info: ';
print_r($recordController->getRecord($id));
echo "<br><br>";

//delete record
echo '<b>Deleting</b> record...<br><br>';
$recordController->deleteRecord($id);

//get all records
echo '<b>Getting all</b> records...<br>';
foreach ($recordController->getAllRecords() as $recordInfo) {
    print_r($recordInfo);
    echo '<br>';
}
echo '<br>';

//get every single record of a technician
echo '<b>Getting all the records of single technician with id 1...</b>...<br>';
foreach ($recordController->getTechnicianFullRecord(1) as $recordInfo) {
    print_r($recordInfo);
    echo '<br>';
}