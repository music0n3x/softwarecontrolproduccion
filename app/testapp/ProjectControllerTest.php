<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");

include '../app.php';
$projectController = new ProjectController();

//create project
echo '<b>Creating</b> project...<br>';
$id = $projectController->createProject($projectController->createDataArray('Daniel', 'rodamientosDaniel.com', date("Y-m-d"), '1', '2'));
echo "Project created with id: $id<br><br>";

//get project
echo '<b>Getting</b> created project info...<br> Project info: ';
print_r($projectController->getProjectInfo($id));
echo "<br><br>";

//update project
echo '<b>Updating</b> project info<br><br>';
$projectController->updateProjectData($projectController->createDataArray('Jose', 'joseInfo.com', date("Y-m-d"), '2', '1', $id));

//get project
echo '<b>Getting</b> updated project info...<br> Project info: ';
print_r($projectController->getProjectInfo($id));
echo "<br><br>";

//delete project
echo '<b>Deleting</b> project...<br><br>';
$projectController->deleteProject($id);

//get all projects
echo '<b>Getting all</b> projects...<br>';
foreach ($projectController->getAllProjects() as $projectInfo) {
    print_r($projectInfo);
    echo '<br>';
}
echo '<br>';

//get client projects
echo '<b>Getting client projects</b> projects...<br>';
foreach ($projectController->getClientProjects(1) as $projectInfo) {
    print_r($projectInfo);
    echo '<br>';
}