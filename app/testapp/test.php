<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");

include '../app.php';
/*
  $taskController = new TaskController();
  $arrayTasksAndProjects = $taskController->getAllProjectTasks();
  $projectController = new ProjectController();
  $outsideLoopProjectId = null;
  foreach ($arrayTasksAndProjects as $tasksAndProject) {
  $projectId = $tasksAndProject['project_id'];
  $outsideLoopProjectId = $tasksAndProject['project_id'];
  $projectWithTasks = array(
  'projectId' => array(),
  'tasks' => array()
  );
  if ($outsideLoopProjectId != $projectId) {
  $project = $projectController->getProjectInfo($projectId);
  array_push($projectWithTasks['projectId'], $projectId);
  }
  array_push($projectWithTasks['tasks'], $tasksAndProject);
  }
 *//*
  $technicianTaskController = new TechnicianTaskController();
  foreach(($technicianTaskController->getTechnicianTasksBy2Clauses('task_id', 1, 'technician_id', 2)) as $technicianTask){
  print_r($technicianTask);
  echo '<br>';
  }
  $technicianTaskController = new TechnicianTaskController();
  foreach($technicianTaskController->getAllInDateRange('2013-07-03', '2013-07-12') as $technicianTask){
  print_r($technicianTask);
  echo '<br>';
  } */
$projectController = new ProjectController();
$technicianTaskController = new TechnicianTaskController();
$taskController = new TaskController();
$logController = new LogController();
/*
  $tasksIdArray = $technicianTaskController->getTaskIdInDateRange("2018-01-22", "2018-01-22");
  print_r($tasksIdArray);
  foreach ($tasksIdArray as $taskId) {
  echo "<br><br><b>Task:</b> <br>";
  $task = $taskController->getTask($taskId['task_id']);
  print_r($task);
  echo "<br><br><br> <b>Technician tasks:</b><br>";
  print_r($technicianTaskController->getAllTechnicianTasksByTaskIdInDateRange("2018-01-22", "2018-01-22", $taskId['task_id']));
  }
 */
/*
  $technicianTaskIdsArray = $technicianTaskController->getIdsInDateRange("2018-01-22", "2018-01-22");
  //$tasksIdArray = $technicianTaskController->getTaskIdsInDateRange("2018-01-22", "2018-01-22");

  $projectsArray = array();

  foreach ($technicianTaskIdsArray as $technicianTask) {
  array_push($projectsArray, TechnicianTaskController::getProjectByTechnicianTaskId($technicianTask['id']));
  }

  $tasksIdArray = Util::delete_duplicates_in_array($technicianTaskController->getTaskIdsInDateRange("2018-12-18", "2018-12-18"));


  $projectsId = array();

  foreach ($tasksIdArray as $taskId) {
  $task = $taskController->getTask($taskId['task_id']);
  array_push($projectsId, $task['project_id']);
  }

  $tasksArray = array();

  foreach ($tasksIdArray as $taskId) {
  $task = $taskController->getTask($taskId['task_id']);
  array_push($tasksArray, $task);
  } */
/*
  $tasksIdArray = Util::delete_duplicates_in_array($technicianTaskController->getTaskIdsInDateRange("2013-07-05", "2013-07-05"));
  $projectsId = array();

  foreach ($tasksIdArray as $taskId) {
  $task = $taskController->getTask($taskId['task_id']);
  array_push($projectsId, $task['project_id']);
  }

  $tasksArray = array();

  foreach ($tasksIdArray as $taskId) {
  $task = $taskController->getTask($taskId['task_id']);
  array_push($tasksArray, $task);
  }
  $defArray = array();

  foreach (Util::delete_duplicates_in_array($projectsId) as $projectId) {
  $projectName = $projectController->getProjectName($projectId);
  $fullInfo = array("projectName" => $projectName, "taskInfo" => array());

  foreach ($taskController->getProjectTasksMatchingTaskId($projectId, $tasksIdArray) as $task) {
  array_push($fullInfo['taskInfo'], array ("taskTitle" => $task['title'], "technicianTasks" => $technicianTaskController->getTechnicianTasksOnDateRangeWhereTaskId("2013-07-05", "2013-07-05", $task['id'])));
  }
  array_push($defArray, $fullInfo);
  }
 */
/*
  $tasksIdArray = $logController->getTechnicianTasksDistinctTasksId(17);
  $projectsId = array();

  foreach ($tasksIdArray as $taskId) {
  $task = $taskController->getTask($taskId);
  array_push($projectsId, $task['project_id']);
  }

  $tasksArray = array();

  foreach ($tasksIdArray as $taskId) {
  $task = $taskController->getTask($taskId['task_id']);
  array_push($tasksArray, $task);
  }
  $defArray = array();

  foreach (Util::delete_duplicates_in_array($projectsId) as $projectId) {
  $projectName = $projectController->getProjectName($projectId);
  $fullInfo = array("projectName" => $projectName, "taskInfo" => array());
  foreach ($taskController->getProjectTasksMatchingTaskId($projectId, $tasksIdArray) as $task) {
  array_push($fullInfo['taskInfo'], array("taskTitle" => $task['title'], "technicianTasks" => $technicianTaskController->getTechnicianTaskByIdAndTaskId($task['id'], 17)));
  }
  array_push($defArray, $fullInfo);
  }
 *//*
  $tasksIdArray = $logController->getTechnicianTasksDistinctTasksId(1);
  print_r($taskController->getProjectTasksMatchingTaskId(239, $tasksIdArray)); */

$tasksIdArray = Util::delete_duplicates_in_array($technicianTaskController->getTaskIdsInDateRange("2018-12-18", "2018-12-18"));
$projectsId = array();

foreach ($tasksIdArray as $taskId) {
    $task = $taskController->getTask($taskId['task_id']);
    array_push($projectsId, $task['project_id']);
}

$tasksArray = array();

foreach ($tasksIdArray as $taskId) {
    $task = $taskController->getTask($taskId['task_id']);
    array_push($tasksArray, $task);
}
$defArray = array();

foreach (Util::delete_duplicates_in_array($projectsId) as $projectId) {
    $projectName = $projectController->getProjectName($projectId);
    $fullInfo = array("projectName" => $projectName, "taskInfo" => array());
    foreach ($taskController->getProjectTasksMatchingTaskId($projectId, $tasksIdArray) as $task) {
        array_push($fullInfo['taskInfo'], array("taskTitle" => $task['title'], "technicianTasks" => $technicianTaskController->getTechnicianTasksOnDateRangeWhereTaskId("2018-12-18", "2018-12-18", $task['id'])));
    }
    array_push($defArray, $fullInfo);
}

//print_r($tasksIdArray);


//$array = $logController->createProjectInfoFiltered($projectsArrayFiltered, $tasksIdArray, $technicianTaskIdsArray);
//print_r(Util::delete_duplicates_in_array_by_key($technicianTaskController->getTaskIdsInDateRange("2018-12-18", "2018-12-18"), 'task_id'));
//print_r($technicianTaskController->getTechnicianTasksOnDateRangeWhereTaskId("2018-12-18", "2018-12-18", 1349));
/*foreach ($tasksIdArray as $taskId) {
    $task = $taskController->getTaskByIdAndProjectId(268, $taskId['task_id']);
    print_r($task);
}/*
//print_r($tasksIdArray);
