<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");

include '../app.php';
$taskController = new TaskController();

//create task
echo '<b>Creating</b> task...<br>';
$id = $taskController->createTask($taskController->createDataArray('danielInformacion', 'LACASADANIEL.com', 5, date("Y-m-d"), date("Y-m-d"), date("Y-m-d"), 1, 1, 1, 1));
echo "Task created with id: $id<br><br>";

//get task
echo '<b>Getting</b> created task info...<br> Task info: ';
print_r($taskController->getTask($id));
echo "<br><br>";

//update task
echo '<b>Updating</b> task info<br><br>';
$taskController->updateTask($taskController->createDataArray('joseInformacion', 'LACASAJOSE.com', 5, date("Y-m-d"), date("Y-m-d"), date("Y-m-d"), 1, 1, 1, 1, $id));

//get task
echo '<b>Getting</b> updated task info...<br> Task info: ';
print_r($taskController->getTask($id));
echo "<br><br>";

//delete task
echo '<b>Deleting</b> task...<br><br>';
$taskController->deleteTask($id);

//get all tasks
echo '<b>Getting all</b> tasks...<br>';
foreach ($taskController->getAllTasks() as $taskInfo) {
    print_r($taskInfo);
    echo '<br>';
}