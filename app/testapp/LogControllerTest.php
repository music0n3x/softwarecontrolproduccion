<?php

ini_set("display_errors", 2);
date_default_timezone_set("Europe/Madrid");

include '../app.php';
$id = 1;
$logController = new LogController();
//get full info of project by technician id
echo '<h3>Getting projects info of technician with id: 1...</h3>';
foreach ($logController->getTechnicianTasksFullInfo($id) as $key => $log) {
    echo '<hr><br>' . 'Record ' . $key . '<br><br>';
    echo '<b>Project</b> => ';
    print_r($log['project']);
    echo '<br><br>';
    echo '<b>Task</b> => ';
    print_r($log['task']);
    echo '<br><br>';
    echo '<b>Technician tasks</b> => ';
    print_r($log['technicianTask']);
    echo '<br><br><br>';
}

echo '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<';
//get project info by project id
echo '<h3>Getting project info with id: ' . $id . '...<br></h3>';
foreach ($logController->getProjectTasks($id) as $key => $log) {
    echo '<hr><br>' . 'Record ' . $key . '<br><br>';
    echo '<b>Task</b> => ';
    print_r($log['task']);
    if (!empty($log['technicianTasks'])) {
        echo '<br><br><b>Technician tasks</b> => ';
        print_r($log['technicianTasks']);
    }
    echo '<br><br>';
}