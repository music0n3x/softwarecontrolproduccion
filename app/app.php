<?php

include(dirname(__FILE__) . "/config/database.php");
include_once(dirname(__FILE__) . "/core/Util.php");
include_once(dirname(__FILE__) . "/core/Database.php");
include_once(dirname(__FILE__) . "/core/Model.php");
include_once(dirname(__FILE__) . "/core/Controller.php");
include_once(dirname(__FILE__) . "/core/Languages.php");
include_once(dirname(__FILE__) . "/lib/SortArrayAlphabetical.php");
include_once(dirname(__FILE__) . "/lib/Parsecsv.php");
include_once(dirname(__FILE__) . "/lib/StaticLang.php");
include_once(dirname(__FILE__) . "/lib/tcpdf/tcpdf.php");
include_once(dirname(__FILE__) . "/lib/PDFGenerator.php");
include_once(dirname(__FILE__) . "/lib/upload/src/class.upload.php");

include_once(dirname(__FILE__) . "/models/Client.php");
include_once(dirname(__FILE__) . "/models/TableToMigrate.php");
include_once(dirname(__FILE__) . "/controllers/ClientController.php");
include_once(dirname(__FILE__) . "/controllers/ProjectController.php");
include_once(dirname(__FILE__) . "/controllers/LogController.php");
include_once(dirname(__FILE__) . "/controllers/RecordController.php");
include_once(dirname(__FILE__) . "/controllers/TaskController.php");
include_once(dirname(__FILE__) . "/controllers/TechnicianTaskController.php");
include_once(dirname(__FILE__) . "/controllers/BillingController.php");
include_once(dirname(__FILE__) . "/controllers/PerformanceController.php");
include_once(dirname(__FILE__) . "/controllers/StatusController.php");
include_once(dirname(__FILE__) . "/controllers/TechnicianController.php");