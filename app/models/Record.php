<?php

class Record extends Model{

    private $id;
    private $finishDate;
    private $startDate;
    private $technicianId;
    
    public function __construct() {
        parent::__construct('v2_record');
    }
    
    //returns record info by id (parameter) in an associative array
    public function getRecordById($id) {
        return $this->getById($id);
    }
    
    //inserts new record on database  
    public function createRecord() {
        $sth = $this->db->prepare("INSERT INTO $this->table (finish_date, start_date, technician_id) "
                . "VALUES ('$this->finishDate', '$this->startDate', '$this->technicianId')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //updates record info
    public function editRecord() {
        $sth = $this->db->prepare("UPDATE $this->table SET finish_date='$this->finishDate', start_date='$this->startDate', technician_id='$this->technicianId'"
                . " WHERE id = '$this->id'");
        $sth->execute();
    }

    //returns all table records in an associative array
    public function getAllRecords() {
        $result = $this->getAll();
        return $result;
    }

    //deletes database record by id (parameter)
    public function deleteRecordById($id) {
        $this->deleteById($id);
    }
    
    public function getId() {
        return $this->id;
    }

    public function getFinishDate() {
        return $this->finishDate;
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function getTechnicianId() {
        return $this->technicianId;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setFinishDate($finishDate) {
        $this->finishDate = $finishDate;
    }

    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    public function setTechnicianId($technicianId) {
        $this->technicianId = $technicianId;
    }

}