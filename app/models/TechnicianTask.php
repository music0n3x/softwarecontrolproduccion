<?php

class TechnicianTask extends Model {

    private $id;
    private $startDate;
    private $minutes;
    private $description;
    private $taskId;
    private $technicianId;

    public function __construct() {
        parent::__construct('v2_technician_task');
    }

    //returns record info by id (parameter) in an associative array
    public function getTechnicianTaskById($id) {
        return $this->getById($id);
    }

    public function getTechnicianTaskIdByTaskId($taskId) {
        return $this->getBy('id', $taskId);
    }
    
    public function getAllTechnicianTasksByTaskId($taskId) {
        return $this->getAllByAltOrdered('task_id', $taskId, "start_date");
    }

    //inserts new record on database
    public function createTechnicianTask() {
        $sth = $this->db->prepare("INSERT INTO $this->table (start_date, minutes, description, task_id, technician_id) "
                . "VALUES ('$this->startDate', '$this->minutes', '$this->description','$this->taskId', '$this->technicianId')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //inserts new record on database with id
    public function createTechnicianTaskWithId() {
        $sth = $this->db->prepare("INSERT INTO $this->table (id, start_date, minutes, description, task_id, technician_id) "
                . "VALUES ('$this->id' ,'$this->startDate', '$this->minutes', '$this->description','$this->taskId', '$this->technicianId')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //updates record info
    public function editTechnicianTask() {
        $sth = $this->db->prepare("UPDATE $this->table SET start_date='$this->startDate', minutes='$this->minutes', description='$this->description',task_id='$this->taskId', technician_id='$this->technicianId'"
                . " WHERE id = '$this->id'");
        $sth->execute();
    }

    //returns all table records in an associative array
    public function getAllTechnicianTasks() {
        $result = $this->getAll();
        return $result;
    }

    //deletes database record by id (parameter)
    public function deleteTechnicianTaskById($id) {
        $this->deleteById($id);
    }

    public function getId() {
        return $this->id;
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function getMinutes() {
        return $this->minutes;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getTaskId() {
        return $this->taskId;
    }

    public function getTechnicianId() {
        return $this->technicianId;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    public function setMinutes($minutes) {
        $this->minutes = $minutes;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setTaskId($taskId) {
        $this->taskId = $taskId;
    }

    public function setTechnicianId($technicianId) {
        $this->technicianId = $technicianId;
    }

}
