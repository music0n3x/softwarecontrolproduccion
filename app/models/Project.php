<?php

class Project extends Model {

    private $id;
    private $name;
    private $info;
    private $createdDate;
    private $active;
    private $clientId;

    public function __construct() {
        parent::__construct('v2_projects');
    }

    //returns record info by id (parameter) in an associative array
    public function getProjectById($id) {
        return $this->getById($id);
        //var_dump();
    }

    //inserts new record on database  
    public function createProject() {
        $sth = $this->db->prepare("INSERT INTO $this->table (name, info, created_date, active, client_id) "
                . "VALUES ('$this->name', '$this->info', '$this->createdDate', '$this->active', '$this->clientId')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //inserts new record on database with id 
    public function createProjectWithId() {      
        $sth = $this->db->prepare("INSERT INTO $this->table (id, name, info, created_date, active, client_id) "
                . "VALUES ('$this->id', '$this->name', '$this->info', '$this->createdDate', '$this->active', '$this->clientId')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //updates record info
    public function editProject() {
        $sth = $this->db->prepare("UPDATE $this->table SET name='$this->name', info='$this->info', created_date='$this->createdDate', active='$this->active', client_id='$this->clientId'"
                . " WHERE id = '$this->id'");
        $sth->execute();
    }

    //returns all table records in an associative array
    public function getAllProjects() {
        $result = $this->getAllOrderBy('id', 'DESC');
        //$result = $this->getAllOrderBy("created_date", "DESC");
        return $result;
    }
    
    //returns all table records in an associative array ordered alphabetically
    public function getAllProjectsAlphabetically() {
        $result = $this->getAllOrderBy('name', 'ASC');
        //$result = $this->getAllOrderBy("created_date", "DESC");
        return $result;
    }

    //deletes database record by id (parameter)
    public function deleteProjectById($id) {
        $this->deleteById($id);
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getInfo() {
        return $this->info;
    }

    public function getCreatedDate() {
        return $this->$createdDate;
    }

    public function getActive() {
        return $this->active;
    }

    public function getClienId() {
        return $this->clientId;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setInfo($info) {
        $this->info = $info;
    }

    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

    public function setActive($active) {
        $this->active = $active;
    }

    public function setClientId($clientId) {
        $this->clientId = $clientId;
    }

}
