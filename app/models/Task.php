<?php

class Task extends Model {

    private $id;
    private $info;
    private $title;
    private $hours;
    private $createdDate;
    private $startDate;
    private $finishDate;
    private $projectId;
    private $statusId;
    private $performanceId;
    private $billingId;

    public function __construct() {
        parent::__construct('v2_tasks');
    }

    //returns record info by id (parameter) in an associative array
    public function getTaskById($id) {
        return $this->getById($id);
    }

    //inserts new record on database
    public function createTask() {
        $sth = $this->db->prepare("INSERT INTO $this->table (info, title, hours, created_date, start_date, finish_date, project_id, status_id, performance_id, billing_id) "
                . "VALUES ('$this->info', '$this->title', '$this->hours', '$this->createdDate', '$this->startDate', '$this->finishDate', '$this->projectId', '$this->statusId', '$this->performanceId', '$this->billingId')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //inserts new record on database with id
    public function createTaskWithId() {
        $sql = "INSERT INTO $this->table (id, info, title, hours, created_date, start_date, finish_date, project_id, status_id, performance_id, billing_id) "
                . "VALUES ('$this->id', '$this->info', '$this->title', '$this->hours', '$this->createdDate', '$this->startDate', '$this->finishDate', '$this->projectId', '$this->statusId', '$this->performanceId', '$this->billingId')";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //updates record info
    public function editTask() {
        $sth = $this->db->prepare("UPDATE $this->table SET info='$this->info', title='$this->title', hours='$this->hours', created_date='$this->createdDate', start_date='$this->startDate', finish_date='$this->finishDate', project_id='$this->projectId', status_id='$this->statusId', performance_id='$this->performanceId', billing_id='$this->billingId'"
                . " WHERE id = '$this->id'");
        $sth->execute();
    }

    //returns all table records in an associative array
    public function getAllTasks() {
        $result = $this->getAllOrderBy("finish_date", "DESC");
        return $result;
    }

    //deletes database record by id (parameter)
    public function deleteTaskById($id) {
        $this->deleteById($id);
    }

    public function getId() {
        return $this->id;
    }

    public function getInfo() {
        return $this->info;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getHours() {
        return $this->hours;
    }

    public function getCreatedDate() {
        return $this->createdDate;
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function getFinishDate() {
        return $this->finishDate;
    }

    public function getProjectId() {
        return $this->projectId;
    }

    public function getStatusId() {
        return $this->statusId;
    }

    public function getPerformance_id() {
        return $this->performanceId;
    }

    public function getBilling_id() {
        return $this->billingId;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setInfo($info) {
        $this->info = $info;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setHours($hours) {
        $this->hours = $hours;
    }

    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    public function setFinishDate($finishDate) {
        $this->finishDate = $finishDate;
    }

    public function setProjectId($projectId) {
        $this->projectId = $projectId;
    }

    public function setStatusId($statusId) {
        $this->statusId = $statusId;
    }

    public function setPerformanceId($performanceId) {
        $this->performanceId = $performanceId;
    }

    public function setBillingId($billingId) {
        $this->billingId = $billingId;
    }

}
