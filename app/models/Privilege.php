<?php

class Privilege extends Model {

    private $id;
    private $level;

    public function __construct() {
        parent::__construct('v2_privileges');
    }

    //returns record info by id (parameter) in an associative array  
    public function getPrivilegeById($id) {
        return $this->getById($id);
    }

    //inserts new record on database    
    public function createPrivilage() {
        $sth = $this->db->prepare("INSERT INTO $this->table (level) "
                . "VALUES ('$this->level')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //inserts new record on database with id
    public function createPrivilageWithId() {
        $sth = $this->db->prepare("INSERT INTO $this->table (id, level) "
                . "VALUES ('$this->id', '$this->level')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //updates record info
    public function editPrivilege() {
        $sth = $this->db->prepare("UPDATE $this->table SET level='$this->level'"
                . " WHERE id = '$this->id'");
        $sth->execute();
    }

    //returns all table records in an associative array 
    public function getAllPrivileges() {
        $result = $this->getAll();
        return $result;
    }

    //deletes database record by id (parameter)
    public function deletePrivilegeById($id) {
        $this->deleteById($id);
    }

    public function getId() {
        return $this->id;
    }

    public function getLevel() {
        return $this->level;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setLevel($level) {
        $this->level = $level;
    }

}
