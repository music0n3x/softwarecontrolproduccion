<?php

class Technician extends Model {

    private $id;
    private $name;
    private $username;
    private $password;
    private $privilegesId;

    public function __construct() {
        parent::__construct('v2_technician');
    }

    //returns record info by id (parameter) in an associative array
    public function getTechnicianById($id) {
        return $this->getById($id);
    }

    public function getPasswordMD5($username, $password) {
        $sth = $this->db->prepare("SELECT id FROM technician WHERE username='%s' and password=MD5('%s')", $username, $password);
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    //inserts new record on database
    public function createTechnician() {
        $sth = $this->db->prepare("INSERT INTO $this->table (name, username, password, privilege_id) "
                . "VALUES ('$this->name', '$this->username'), '$this->password', '$this->privilegesId'");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //inserts new record on database with id
    public function createTechnicianWithId() {
        $sth = $this->db->prepare("INSERT INTO $this->table (id, name, username, password, privilege_id) "
                . "VALUES ('$this->id', '$this->name', '$this->username', '$this->password', '$this->privilegesId')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //updates record info
    public function editTechnician() {
        $sth = $this->db->prepare("UPDATE $this->table SET name='$this->name', username='$this->username', password='$this->password', privilege_id='$this->privilegesId'
"
                . " WHERE id = '$this->id'");
        $sth->execute();
    }

    //returns all table records in an associative array
    public function getAllTechnician() {
        $result = $this->getAll();
        return $result;
    }
    
    //returns all table records in an associative array ordered alphabetically
    public function getAllTechnicianAlphabetically() {
        $result = $this->getAllAlphabetically("name");
        return $result;
    }

    //deletes database record by id (parameter)
    public function deleteTechnicianById($id) {
        $this->deleteById($id);
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getPrivilegesId() {
        return $this->privilegesId;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setPrivilegesId($privilegesId) {
        $this->privilegesId = $privilegesId;
    }

}
