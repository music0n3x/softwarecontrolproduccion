<?php

class Billing extends Model {

    private $id;
    private $description;

    public function __construct() {
        parent::__construct("v2_billing");
    }
    
    //returns record info by id (parameter) in an associative array
    public function getBillingById($id) {
        return $this->getById($id);
    }
    
    //inserts new record on database
    public function createBilling() {
        $sth = $this->db->prepare("INSERT INTO $this->table (description) "
                . "VALUES ('$this->description')");
        $sth->execute();
        return $this->db->lastInsertId();
    }
    
    //inserts new record on database with id
    public function createBillingWithId() {
        $sth = $this->db->prepare("INSERT INTO $this->table (id, description) "
                . "VALUES ('$this->id', '$this->description')");
        $sth->execute();
        return $this->db->lastInsertId();
    }
    
    //updates record info by id
    public function editBilling() {
        $sth = $this->db->prepare("UPDATE $this->table SET description='$this->description'"
                . " WHERE id = '$this->id'");
        $sth->execute();
    }
    
    //returns all table records in an associative array
    public function getAllBillings() {
        $result = $this->getAll();
        return $result;
    }
    
    //deletes database record by id (parameter)
    public function deleteBillingById($id) {
        $this->deleteById($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

}
