<?php

header('Content-type: text/html; charset=utf-8');

class Client extends Model {

    private $id;
    private $name;
    private $info;

    public function __construct() {
        parent::__construct("v2_clients");
    }

    //returns record info by id (parameter) in an associative array
    public function getClientById($id) {
        return $this->getById($id);
    }

    //inserts new record on database
    public function createClient() {
        $sth = $this->db->prepare("INSERT INTO $this->table (name, info) "
                . "VALUES ('$this->name', '$this->info')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //inserts new record on database with id
    public function createClientWithId() {
        $sth = $this->db->prepare("INSERT INTO $this->table (id, name, info) "
                . "VALUES ('$this->id', '$this->name', '$this->info')");
        $sth->execute();
        return $this->db->lastInsertId();
    }

    //updates record info
    public function editClient() {
        $sth = $this->db->prepare("UPDATE $this->table SET name='$this->name', info='$this->info'"
                . " WHERE id = '$this->id'");
        $sth->execute();
    }

    //returns all table records in an associative array    
    public function getAllClients() {
        $result = $this->getAll();
        return $result;
    }

    //deletes database record by id (parameter)
    public function deleteClientById($id) {
        $this->deleteById($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getInfo() {
        return $this->info;
    }

    public function setInfo($info) {
        $this->info = $info;
    }

}
