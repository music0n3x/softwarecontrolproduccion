<?php

session_start();
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    include SECTION_HANDLER;
} else {
    if ($sec != "login") {
        Util::redirect(Util::generateRoute("login", "loginForm"));
    } else {
        include SECTION_HANDLER;
    }
}
