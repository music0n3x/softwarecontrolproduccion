<?php

if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/layout/backend/lang/$lang.xml")) {
    $langXML = simplexml_load_file($_SERVER["DOCUMENT_ROOT"] . "/layout/backend/lang/$lang.xml");
} else {
    exit("Error loading $lang.xml.");
}

define("LANG", "lang=" . $lang);
