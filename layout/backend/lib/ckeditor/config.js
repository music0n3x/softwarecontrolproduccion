/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.extraPlugins = 'templates,magicline,oembed,dialog,fakeobjects,iframe,justify';
    config.image_prefillDimensions = false;
    config.extraAllowedContent = 'div blockquote(*)[*]';
    config.templates_replaceContent = false;
    config.filebrowserBrowseUrl = 'lib/responsiveFilemanager/dialog.php?type=2&editor=ckeditor&multiple=0&fldr=';
    config.filebrowserUploadUrl = 'lib/responsiveFilemanager/dialog.php?type=2&editor=ckeditor&multiple=0&fldr=';
    config.filebrowserImageBrowseUrl = 'lib/responsiveFilemanager/dialog.php?type=2&editor=ckeditor&multiple=0&fldr=';

    config.magicline_everywhere = true;
    config.internalPages = $.getJSON('ajax/pagesJSON.php', {lang: this.name.split("_").pop()});

    config.toolbar_Contenido = [
        {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
        {name: 'document', items: ['Source', '-', 'Templates', 'Maximize']},
        '/',
        {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat', 'TextColor']},
        {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
        {name: 'links', items: ['Link', 'Unlink']},
        '/',
        {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar', '-', 'Iframe', 'oembed']},
        {name: 'styles', items: ['Format']}
    ];
    config.toolbar_Basic = [
        {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
        {name: 'document', items: ['Source']},
        '/',
        {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
        {name: 'paragraph', items: ['NumberedList', 'BulletedList']},
        {name: 'links', items: ['Link', 'Unlink']}
    ];

    //config.removeButtons = 'SpellChecker,Print,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Font,BGColor,About,Styles';
};