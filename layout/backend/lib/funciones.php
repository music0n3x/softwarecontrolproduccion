<?php

function checkbox_value($checkbox) {
    if (empty($checkbox)) {
        return 0;
    }
    return 1;
}

function option_selected($option_id, $item_id) {
    if ($option_id == $item_id) {
        return 'selected="selected"';
    }
}
function option_category_selected($category_id, $category_selected, $parent){
    if($category_id == $category_selected && $parent == 0){
        return 'selected="selected"';
    }
}
function input_checked($input_value) {
    if ($input_value || $input_value == 1) {
        return 'checked="checked"';
    }
}

function http_to_url($url) {
    if ($url != "") {
        if (preg_match("#https?://#", $url) === 0) {
            $url = 'http://' . $url;
        }
    }
    return $url;
}

function hide_item($hide, $attrClass = false) {
    if ($hide) {
        if ($attrClass) {
            echo 'class="hide"';
        } else {
            echo 'hide';
        }
    }
}

function prepare_to_db($data, $is_html = false) {
    global $link;

    if ($is_html) {
        $data = mysqli_real_escape_string($link, $data);
    } else {
        $data = html2txt($data);
        $data = mysqli_real_escape_string($link, $data);
    }

    return $data;
}

function fecha_to_mysql($fecha) {
    $trozos = explode("/", $fecha);
    $nova_fecha = $trozos[2] . "-" . $trozos[1] . "-" . $trozos[0];
    return $nova_fecha;
}

//-------------  2008-12-12 12:06:30  PASSAR A  12:06 -------------------------------##
function formato_hora($fecha) {
    $hora = substr($fecha, 11, 5);

    return $hora;
}

function obtener_hora($fecha) {
    $hora = substr($fecha, 11, 2);

    return $hora;
}

function obtener_minuto($fecha) {
    $minuto = substr($fecha, 14, 2);

    return $minuto;
}

//-------------  2008-12-12 12:06:30  PASSAR A  12/12/2008 12:06 -------------------------------##
function fecha_larga_view($fecha) {
    $trozos = explode(" ", $fecha);
    $dia = $trozos[0];
    $hora = $trozos[1];

    $trozos_dia = explode("-", $dia);
    $nueva_fecha_dia = $trozos_dia[2] . "/" . $trozos_dia[1] . "/" . $trozos_dia[0];

    $nueva_hora = substr($hora, 0, 5);

    $nueva_fecha_foro = $nueva_fecha_dia . " " . $nueva_hora;
    return $nueva_fecha_foro;
}

/* -------------------------------------------------- marcar dates caducades -- */

function dataPublicada($fecha2, $public) {
    $fecha1 = strtotime(date("Y-m-d"));
    $fecha2 = strtotime($fecha2);

    if ($public == "caducada" && $fecha1 >= $fecha2) {
        return "caducada";
    }
    if ($public == "publicada" && $fecha1 >= $fecha2) {
        return "sin_publicar";
    }
}