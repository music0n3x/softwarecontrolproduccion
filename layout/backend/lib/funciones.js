function contarCaracteres(texto, numCaracteres_txt, avisoCaracteres_txt, max) {
    textoArea = document.getElementById(texto).value;
    numeroCaracteres = textoArea.length;

    //no contar els <strong></strong>
    var i = 0;
    var counter1 = 0;
    while (i != -1) {
        i = textoArea.indexOf("<strong>", i);
        if (i != -1) {
            i++;
            counter1++;
        }
    }
    var x = 0;
    var counter2 = 0;
    while (x != -1) {
        x = textoArea.indexOf("</strong>", x);
        if (x != -1) {
            x++;
            counter2++;
        }
    }
    var caracteresStrong = (counter1 * 8) + (counter2 * 9);
    document.getElementById(numCaracteres_txt).value = (numeroCaracteres - caracteresStrong) + " caracteres";
    if (numeroCaracteres > max) {
        document.getElementById(avisoCaracteres_txt).style.display = 'block';
    } else {
        document.getElementById(avisoCaracteres_txt).style.display = 'none';
    }
}
/* funcion que comprueba la extension */
function comprueba_extension(formulario, archivo) {
    extensiones_permitidas = new Array(".pdf");
    mierror = "";
    if (!archivo) {
        //Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
        mierror = "No has seleccionado ningún archivo";
    } else {
        extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
        permitida = false;
        for (var i = 0; i < extensiones_permitidas.length; i++) {
            if (extensiones_permitidas[i] == extension) {
                permitida = true;
                break;
            }
        }
        if (!permitida) {
            mierror = "Sólo se pueden subir archivos con extensiones: " + extensiones_permitidas.join();
        } else {
            formulario.submit();
            return 1;
        }
    }
    alert(mierror);
    return 0;
}

function comprueba_extension_img(formulario, archivo) {
    extensiones_permitidas = new Array(".jpeg", ".jpg");
    mierror = "";
    if (!archivo) {
        //Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
        //mierror = "No has seleccionado ningún archivo";
        formulario.submit();
        return 1;
    } else {
        extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
        permitida = false;
        for (var i = 0; i < extensiones_permitidas.length; i++) {
            if (extensiones_permitidas[i] == extension) {
                permitida = true;
                break;
            }
        }
        if (!permitida) {
            mierror = "Sólo se pueden subir archivos con extensiones: " + extensiones_permitidas.join();
        } else {
            formulario.submit();
            return 1;
        }
    }
    alert(mierror);
    return 0;
}

function validarfecha(Cadena) {
    var Fecha = new String(Cadena)   // Crea un string
    var RealFecha = new Date()   // Para sacar la fecha de hoy

    // Cadena Año
    var Ano = new String(Fecha.substring(Fecha.lastIndexOf("/") + 1, Fecha.length))
    // Cadena Mes
    var Mes = new String(Fecha.substring(Fecha.indexOf("/") + 1, Fecha.lastIndexOf("/")))
    // Cadena Día
    var Dia = new String(Fecha.substring(0, Fecha.indexOf("/")))

    // Valido el año
    if (isNaN(Ano) || Ano.length < 4 || parseFloat(Ano) < 1900) {
        return false;
    }
    // Valido el Mes
    if (isNaN(Mes) || parseFloat(Mes) < 1 || parseFloat(Mes) > 12) {
        return false;
    }
    // Valido el Día
    if (isNaN(Dia) || parseInt(Dia, 10) < 1 || parseInt(Dia, 10) > 31) {
        return false;
    }
    if (Mes == 4 || Mes == 6 || Mes == 9 || Mes == 11 || Mes == 2) {
        if (Mes == 2 && Dia > 28 || Dia > 30) {
            return false;
        }
    }

    return true;
}

function validarhora(str) {
    hora = str;
    if (hora.length != 5) {
        return false;
    }
    a = hora.charAt(0)
    b = hora.charAt(1)
    c = hora.charAt(2)
    if (a >= 2 && b > 3) {
        return false;
    }
    if (c > 5) {
        return false;
    }

    return true;
}


function validarEmail(valor) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(valor)) {
        return (true);
    } else {
        return (false);
    }
}

function validaURL(url) {
    var re = /^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)( [a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_]*)?$/;
    return re.test(url);
}

function isNum(q) {
    if (isNaN(q)) {
        return false;
    }
    return true;
}

function exists(selector) {
    if ($(selector).length > 0) {
        return true;
    }
    return false;
}