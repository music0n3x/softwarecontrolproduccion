<?php

function human_filesize($bytes, $decimals = 2) {
    $sz = 'BKMGTP';
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}

function dirtree($dir, $ignore, $regex = '', $ignoreEmpty = true) {
    if (!$dir instanceof DirectoryIterator) {
        $dir = new DirectoryIterator((string) $dir);
    }
    $dirs = array();
    $files = array();
    foreach ($dir as $node) {
        if (!in_array($node->getFilename(), $ignore)) {
            if ($node->isDir() && !$node->isDot()) {
                $tree = dirtree($node->getPathname(), $ignore, $regex, $ignoreEmpty);
                if (!$ignoreEmpty || count($tree)) {
                    $dirName = DS_DIR_PREFIX . $node->getFilename();
//                    echo "dir -->".$dirName."<--";
                    $dirs[$dirName] = $tree;
                }
            } elseif ($node->isFile()) {
                $name = $node->getFilename();
                if ('' == $regex || preg_match($regex, $name)) {
                    $files[] = $name;
                }
            }
        }
    }
    asort($dirs);
    sort($files);
    return array_merge($dirs, $files);
}

function array_find_deep($array, $search, $keys = array(), &$main = array()) {

    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $sub = array_find_deep($value, $search, array_merge($keys, array($key)), $main);
            /* if (count($sub)) {
              return $sub;
              } */
        } elseif (stripos($value, $search) !== false) {
            $merge = array_merge($keys, array($value));
            $main[] = $merge;
//                        array_find_deep($array, $search, "", $main);
//                        return array_merge($keys, array($key));
        }
        array_shift($array);
//                    array_find_deep($array, $search, "", $main);
    }

    return $main;
}

function format_search_result($fullPath, $subPath, $searchResult, $baseUploadUrl) {
    $main = array();
    foreach ($searchResult as $result) {
        $aux = array();
        $link = "";
        foreach ($result as $key => $value) {
            $value = str_replace(DS_DIR_PREFIX, "", $value);
            $link .= "/" . $value;
        }
        $link = ltrim($link, "/");
        $completePath = $fullPath . $link;
        $file = end($result);
        $subFolder = str_replace($file, "", $subPath . $link);
        $file_ext = substr(strrchr($file, '.'), 1);
        $filesize = filesize($completePath);
        $filedate = filemtime($completePath);
        list($width, $height) = getimagesize($completePath);

        $aux["fullUrl"] = $baseUploadUrl . $subPath . $link;
        $aux["fullPath"] = $completePath;
        $aux["subFolder"] = $subFolder;
        $aux["subPath"] = $subPath . $link;
        $aux["file"] = $file;
        $aux["extension"] = $file_ext;
        $aux["filesize"] = $filesize;
        $aux["filesize_read"] = human_filesize($filesize);
        $aux["dimension"] = $width . "x" . $height;
        $aux["filedate"] = $filedate;
        $aux["filedate_read"] = date("d-m-Y", $filedate);

        $main[] = $aux;
    }
    return $main;
}
