<?php
$ds_lang = array(
    "es" => array(
        "busqueda_sin_resultados" => "No se han encontrado resultados para el criterio de búsqueda en esta carpeta ni en las internas.",
        "confirmar_borrar" => "¿Seguro que deseas eliminar este archivo?",
        "vista_previa" => "Vista previa",
        "renombrar" => "Renombrar",
        "descargar" => "Descargar"
    ),
    "ca" => array(
        "busqueda_sin_resultados" => "No s'han trobat resultats per al criteri de recerca a aquesta carpeta ni a les internes.",
        "confirmar_borrar" => "Segur que desitjes esborrar aquest arxiu?",
        "vista_previa" => "Vista prèvia",
        "renombrar" => "Renombrar",
        "descargar" => "Descarregar"
    ),
    "en" => array(
        "busqueda_sin_resultados" => "No results were found for the search criteria in this folder or in the internal ones.",
        "confirmar_borrar" => "Are you sure you want to delete this file?",
        "vista_previa" => "Preview",
        "renombrar" => "Rename",
        "descargar" => "Download"
    ),
);