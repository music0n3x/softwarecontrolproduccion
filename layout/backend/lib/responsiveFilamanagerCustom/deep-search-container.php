<?php 
$customVersion = "2.0";
?>
<link href="../responsiveFilamanagerCustom/custom.min.css?v=<?php echo $customVersion; ?>" rel="stylesheet" type="text/css" />
<script src="../responsiveFilamanagerCustom/include-custom.js?v=<?php echo $customVersion; ?>"></script>
<script src="../responsiveFilamanagerCustom/custom.js?v=<?php echo $customVersion; ?>"></script>

<div class="deepSearch_area loading">
    <div class="ds_frame">
        <button class="btn btn-inverse close-deepSearch"><i class="icon-backward icon-white"></i> <?php echo trans('Return_Files_List') ?></button>
        <header class="deepSearch_area_header">
            <form id="ds_form_popup" class="ds_form">
                <input type="text" id="ds_searchTerm_popup" class="ds_searchTerm" value="" />
                <input type="hidden" id="ds_current_path" name="ds_current_path" value="<?php echo $config['current_path']; ?>" />
                <input type="hidden" id="ds_subfolder" name="ds_subfolder" value="<?php echo $rfm_subfolder; ?>" />
                <input type="hidden" id="ds_subdir" name="ds_subdir" value="<?php echo $subdir; ?>" />
                <button type="submit"><i class="icon-folder-open"></i></button>
            </form>
            <div id="ds_multiple-selection">
                <?php if ($config['multiple_selection']) { ?>
                    <?php if ($config['delete_files']) { ?>
                        <button class="tip btn ds_multiple-delete-btn" title="<?php echo trans('Erase'); ?>" data-confirm="<?php echo trans('Confirm_del'); ?>"><i class="icon-trash"></i></button>
                    <?php } ?>
                    <button class="tip btn ds_multiple-select-btn" title="<?php echo trans('Select_All'); ?>"><i class="icon-check"></i></button>
                    <button class="tip btn ds_multiple-deselect-btn" title="<?php echo trans('Deselect_All'); ?>"><i class="icon-ban-circle"></i></button>
                    <?php if ($apply_type != "apply_none" && $config['multiple_selection_action_button']) { ?>
                        <button class="btn multiple-action-btn btn-inverse" data-function="<?php echo $apply_type; ?>"><?php echo trans('Select'); ?></button>
                    <?php } ?>
                <?php } ?>
            </div>
        </header>
        <div class="ds_overflower">
            <div class="ds_container">

            </div>
        </div>
    </div>
</div>