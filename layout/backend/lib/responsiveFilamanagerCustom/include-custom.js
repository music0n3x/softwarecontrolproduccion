function ds_lightbox() {
    $('.ds_container figcaption').on('click', '.preview', function (event) {
        event.preventDefault();
        var _this = $(this);
        if (_this.hasClass('disabled') == false) {
            $('#full-img').attr('src', decodeURIComponent(_this.attr('data-url')));
        }
        return true;
    });
}
function ds_download() {
    $('.ds_container figcaption').on('click', '.ds_download', function (event) {
        event.preventDefault();
        var _this = $(this);
        var formId = _this.attr("data-form");
        var formObj = $("#" + formId);
        formObj.submit();
        return true;
    });
}
function ds_delete() {
    $('.ds_container figcaption').on('click', '.delete-file', function (event) {
        event.preventDefault();
        var _this = $(this);
        var path = _this.closest('figure').attr('data-path');
        bootbox.confirm(_this.attr('data-confirm'), $('#cancel').val(), $('#ok').val(), function (result) {
            if (result == true) {
                ds_execute_action('delete_file', path, '', '', '');
                var fil = $('#files_number');
                fil.text(parseInt(fil.text()) - 1);
                $("figure[data-path='" + path + "']").closest("li").hide(200);
            }
        });
    });
}

function ds_execute_action(action, file, name, container, function_name) {
    if (name !== null)
    {
        name = fix_filename(name);
        $.ajax({
            type: "POST",
            url: "execute.php?action=" + action,
            data: {
                path: file,
                name: name.replace('/', '')
            }
        }).done(function (msg)
        {
            if (msg != "")
            {
                bootbox.alert(msg);
                return false;
            } else
            {
                if (function_name != "")
                {
                    window[ function_name ](container, name);
                }
            }
            return true;
        });
    }
}


function fix_filename(stri) {
    if (stri != null) {
        if ($('#transliteration').val() == "true") {
            stri = replaceDiacritics(stri);
            stri = stri.replace(/[^A-Za-z0-9\.\-\[\] _]+/g, '');
        }
        if ($('#convert_spaces').val() == "true") {
            stri = stri.replace(/ /g, $('#replace_with').val());
        }
        if ($('#lower_case').val() == "true") {
            stri = stri.toLowerCase();
        }
        stri = stri.replace('"', '');
        stri = stri.replace("'", '');
        stri = stri.replace("/", '');
        stri = stri.replace("\\", '');
        stri = stri.replace(/<\/?[^>]+(>|$)/g, "");
        return $.trim(stri);
    }
    return null;
}

function replaceDiacritics(s) {
    var s;

    var diacritics = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g // C, c
    ];

    var chars = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];

    for (var i = 0; i < diacritics.length; i++) {
        s = s.replace(diacritics[ i ], chars[ i ]);
    }

    return s;
}

function ds_renameFile() {
    $('.ds_container figcaption').on('click', '.rename-file', function (event) {
        event.preventDefault();
        var _this = $(this);
        var file_container = _this.closest('figure');
        var path = file_container.attr('data-path');
        var file_title = file_container.find('h4');
        var old_name = $.trim(file_title.text());
        bootbox.prompt($('#rename').val(), $('#cancel').val(), $('#ok').val(), function (name) {
            if (name !== null) {
                name = fix_filename(name);
                if (name != old_name) {
                    ds_execute_action('rename_file', path, name, file_container, 'apply_file_rename');
                }
            }
        }, old_name);
    });
}

function ds_getLink($trigger) {
//    return $trigger.find("figure").attr("data-path");
    return $trigger.find("figure").attr("data-url");
}
function ds_close_window() {
    if ($('#popup').val() == 1) {
        window.close();
    } else {
        if (typeof parent.jQuery(".modal:has(iframe)").modal == "function") {
            parent.jQuery(".modal:has(iframe)").modal("hide");
        }
        if (typeof parent.jQuery !== "undefined" && parent.jQuery)
        {
            if (typeof parent.jQuery.fancybox == 'object') {
                parent.jQuery.fancybox.getInstance().close();
            } else if (typeof parent.jQuery.fancybox == 'function') {
                parent.jQuery.fancybox.close();
            }
        } else
        {
            if (typeof parent.$.fancybox == 'function') {
                parent.$.fancybox.close();
            }
        }
    }
}
function ds_select_image($trigger) {
    var file = $trigger.closest(".file");
    var url = ds_getLink(file);
    var external = $('#field_id').val();
    var windowParent;
    var is_return_relative_url = $('#return_relative_url').val();

    if (is_return_relative_url === "1") {
        url = url.replace($('#base_url').val(), '');
        url = url.replace($('#cur_dir').val(), '');
    }
    console.log(url);
    if ($('#popup').val() == 1)
    {
        windowParent = window.opener;
    } else
    {
        windowParent = window.parent;
    }
    if (external != "")
    {
        if ($('#crossdomain').val() == 1)
        {
            windowParent.postMessage({
                sender: 'responsivefilemanager',
                url: url,
                field_id: external
            },
                    '*'
                    );
        } else {
            var target = $('#' + external, windowParent.document);
            target.val(url).trigger('change');
            if (typeof windowParent.responsive_filemanager_callback == 'function')
            {
                windowParent.responsive_filemanager_callback(external);
            }
            //            ds_close_window();
        }
    } else
    {
        apply_any(url);
    }
}

/*---------------------------------------------------------------- multiple --*/
function ds_multiDelete() {
    $('.ds_multiple-delete-btn').on('click', function () {
        if ($('.selection:checkbox:checked:visible').length == 0) {
            return;
        }
        var _this = $(this);
        bootbox.confirm(_this.attr('data-confirm'), $('#cancel').val(), $('#ok').val(), function (result)
        {
            if (result == true)
            {
                var files = ds_getFiles(true);

                ds_execute_multiple_action('delete_files', files, '', '', '');
                var fil = $('#files_number');
                fil.text(parseInt(fil.text()) - files.length);
                $('.selection:checkbox:checked:visible').each(function () {
                    var path = $(this).closest("figure").attr("data-path");
//                    $(this).closest('li').remove();
                    $("figure[data-path='" + path + "']").closest("li").hide(200);
                });
                $("#multiple-selection").hide(300);
            }
        });
    });
}
function ds_getFiles(path) {
    var files = [];
    $('.selection:checkbox:checked:visible').each(function () {
        var file = $(this).val();
        if (path) {
            file = $(this).closest('figure').attr('data-path');
        }
        files.push(file);
    });
    return files;
}
function ds_execute_multiple_action(action, files, names, container, function_name) {
    if (name !== null)
    {
        name = fix_filename(name);
        $.ajax({
            type: "POST",
            url: "execute.php?action=" + action,
            data: {
                path: files[0],
                paths: files,
                names: names
            }
        }).done(function (msg)
        {
            if (msg != "")
            {
                bootbox.alert(msg);
                return false;
            } else
            {
                if (function_name != "")
                {
                    window[ function_name ](container, name);
                }
            }
            return true;
        });
    }
}