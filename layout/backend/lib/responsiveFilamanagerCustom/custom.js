/*------------------------------------------------------------- deep search --*/
function deep_search_setup() {
    ds_markup();
    ds_events();
}
function ds_markup() {
    var sibling = $(".span6.types");
    sibling.removeClass("span6").addClass("span4");
    var searchBoxArea = $("<div>", {class: "span2 ds_searchBox"});
    var searchForm = $("<form>", {id: "ds_form", class: "ds_form"})
    var searchBox = $("<input>", {type: "text", name: "ds_searchTerm", id: "ds_searchTerm", class: "ds_searchTerm filter-input filter-input-notype", placeholder: "Buscar en carpetas"});
    var iconFolder = $("<i>", {class: "icon-folder-open"});
    var submitBtn = $("<button>", {type: "submit"});
    searchBox.appendTo(searchForm);
    iconFolder.appendTo(submitBtn);
    submitBtn.appendTo(searchForm);
    searchForm.appendTo(searchBoxArea);
    searchBoxArea.insertBefore(sibling);
}
function ds_events() {
    $(".close-deepSearch").on("click", function () {
        $(".deepSearch_area").hide(300, function () {
            $(".ds_container", this).empty();
            $("body").removeClass("ds_scrollLock");
        });
    });
    $(".ds_form").on("submit", function (event) {
        event.preventDefault();
        var searchTerm = $(".ds_searchTerm", this).val();
        if (searchTerm !== "") {
            ds_display(searchTerm);
        }
    });
}

function ds_display(searchTerm) {
    var popup = $(".deepSearch_area");
    $(".ds_searchTerm", popup).val(searchTerm);
    $("body").addClass("ds_scrollLock");
    popup.show(300, function () {
        ds_search_result(popup, searchTerm);
    });
}

function ds_search_result(popup, searchTerm) {
    var file = "../responsiveFilamanagerCustom/deep-search-result.php";
    var current_path = $("#ds_current_path").val();
    var rfm_subfolder = $("#ds_subfolder").val();
    var subdir = $("#ds_subdir").val();
    $(".ds_container", popup).load(file, {searchTerm: searchTerm, current_path: current_path, rfm_subfolder: rfm_subfolder, subdir: subdir, multiple: MULTIPLE}, function (data) {
        ds_lightbox();
        ds_download();
        ds_delete();
        ds_renameFile();
        if (MULTIPLE === "1") {
            ds_multiple_events();
        } else {
            ds_select_single();
        }
        $(".deepSearch_area.loading").removeClass("loading");
    });
}
function ds_select_single() {
    $(".ds_container").on("click", ".img-precontainer", function (event) {
        event.preventDefault();
        ds_select_image($(this));
    });
}
function ds_multiple_events() {
    $(".ds_multiple-select-btn").on("click", function (event) {
        event.preventDefault();
        $(".ds_container :checkbox").prop("checked", true);
    });
    $(".ds_multiple-deselect-btn").on("click", function (event) {
        event.preventDefault();
        $(".ds_container :checkbox").prop("checked", false);
    });
    ds_multiDelete();
}



/*------------------------------------------------------- loading animation --*/
function startLoadingAnimation(item) {
    var li = item.closest("li");
    var ul = item.closest("ul");
    var cover = $("<li>", {id: "itemsCover"});
    cover.prependTo(ul);
    li.attr("id", "itemActive");
}

var MULTIPLE;
$(document).ready(function () {
    MULTIPLE = $("#multiple").val();
    if (MULTIPLE === "1") {
        $("body").on("click", ".file .img-precontainer", function (event) {
            event.preventDefault();
            $(":checkbox", this).prop("checked", true);
        });
    }
    $(".folder-link").on("click", function (event) {
        startLoadingAnimation($(this));
    });
    deep_search_setup();
//    ds_display("cartel");
});