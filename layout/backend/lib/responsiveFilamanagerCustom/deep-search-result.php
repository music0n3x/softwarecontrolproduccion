<?php
include "functions.php";
include "lang.php";
include "../responsiveFilemanager/config/config.php";

$ds_dl = $config["default_language"];
define("DS_DIR_PREFIX", "DS_DIRNAME-"); //pr carpetes amb números en compte string

$searchTerm = $_POST["searchTerm"];
$currentPath = $_POST["current_path"];
$rfm_subfolder = $_POST["rfm_subfolder"];
$subdir = $_POST["subdir"];
$multiple = $_POST['multiple'];

$fullPath = $currentPath . $rfm_subfolder . $subdir;
$subPath = $rfm_subfolder . $subdir;
$baseUploadUrl = $config["base_url"].$config["upload_dir"];

$folderArray = dirtree($fullPath, $config['hidden_folders']);
$searchResult = array_find_deep($folderArray, $searchTerm);
$formattedSearchResult = format_search_result($fullPath, $subPath, $searchResult, $baseUploadUrl);
//echo "<pre>";
//print_r($formattedSearchResult);
//echo "</pre>";
?>

<?php if (!empty($formattedSearchResult)) { ?>
    <ul class="grid cs-style-2 list-view0" id="searchResult_list">
        <?php
        foreach ($formattedSearchResult as $key => $item) {
            $is_icon_thumb = false;

            // és afoto
            if (in_array($item['extension'], $config['ext_img'])) {
                $src_thumb = $config["thumbs_base_path"] . $item["subPath"];
            } else {
                // no és afoto
                $src_thumb = '../responsiveFilemanager/img/' . $config['icon_theme'] . '/' . $item['extension'] . ".jpg";
                $is_icon_thumb = true;
            }
            if (!file_exists($src_thumb)) {
                $src_thumb = "../responsiveFilemanager/img/" . $config['icon_theme'] . "/default.jpg";
                $is_icon_thumb = true;
            }
            ?>
            <li class="ff-item-type-2 file" data-name="<?php $item["file"]; ?>">		
                <figure data-name="<?php echo $item["file"]; ?>" data-url="<?php echo $item["fullUrl"]; ?>" data-path="<?php echo $item["subPath"]; ?>" data-type="img">
                    <a href="javascript:void('')" class="link" data-file="<?php echo $item["file"]; ?>" data-function="apply_none">
                        <div class="img-precontainer">
                            <?php if ($is_icon_thumb) { ?><div class="filetype"><?php echo $item['extension'] ?></div><?php } ?>
                            <?php if ($multiple == 1) { ?>
                                <div class="selector">
                                    <label class="cont">
                                        <input type="checkbox" class="selection" name="selection[]" value="<?php echo $item["file"]; ?>">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            <?php } ?>
                            <div class="img-container">
                                <img class="loaded" data-src="<?php echo $src_thumb; ?>" src="<?php echo $src_thumb; ?>" data-was-processed="true">
                            </div>
                        </div>
                        <div class="img-precontainer-mini original-thumb">
                            <div class="selector">
                                <label class="cont">
                                    <input type="checkbox" class="selection" name="selection[]" value="<?php echo $item["file"]; ?>">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="filetype jpg  hide">jpg</div>
                            <div class="img-container-mini">
                                <img class="" data-src="<?php echo $src_thumb; ?>">
                            </div>
                        </div>
                        <?php if ($is_icon_thumb) { ?>
                            <div class="cover"></div>
                        <?php } ?>
                        <div class="box">
                            <h4 class="ellipsis"><?php echo $item["file"]; ?></h4>
                        </div></a>
                    <input type="hidden" class="date" value="<?php echo $item["filedate"]; ?>">
                    <input type="hidden" class="size" value="<?php echo $item["filesize"]; ?>">
                    <input type="hidden" class="extension" value="<?php echo $item["extension"]; ?>">
                    <input type="hidden" class="name" value="<?php echo $item["file"]; ?>">
                    <div class="file-date"><?php echo $item["filedate_read"]; ?></div>
                    <div class="file-size"><?php echo $item["filesize_read"]; ?></div>
                    <div class="img-dimension"><?php echo $item["dimension"]; ?></div>
                    <div class="file-extension"><?php echo $item["extension"]; ?></div>
                    <figcaption>
                        <form action="force_download.php" method="post" class="download-form" id="form_ds_<?php echo $key; ?>">
                            <input type="hidden" name="path" value="<?php echo $item["subFolder"]; ?>">
                            <input type="hidden" class="name_download" name="name" value="<?php echo $item["file"]; ?>">

                            <a title="" class="tip-right ds_download" href="" data-form="form_ds_<?php echo $key; ?>" data-original-title="<?php echo $ds_lang[$ds_dl]["descargar"]; ?>"><i class="icon-download "></i></a>

                            <a class="tip-right preview" title="" data-url="<?php echo $config["current_path"] . $item["subPath"]; ?>" data-toggle="lightbox" href="#previewLightbox" data-original-title="<?php echo $ds_lang[$ds_dl]["vista_previa"]; ?>"><i class=" icon-eye-open"></i></a>
                            <a href="javascript:void('')" class="tip-left edit-button rename-file-paths rename-file" title="" data-folder="0" data-permissions="" data-original-title="<?php echo $ds_lang[$ds_dl]["renombrar"]; ?>">
                                <i class="icon-pencil "></i></a>

                            <a href="javascript:void('')" class="tip-left erase-button delete-file" title="" data-confirm="<?php echo $ds_lang[$ds_dl]["confirmar_borrar"]; ?>" data-original-title="Eliminar">
                                <i class="icon-trash "></i>
                            </a>
                        </form>
                    </figcaption>
                </figure>
            </li>
        <?php }
        ?>
    </ul>
<?php } else { ?>
    <div class="alert-info alert"><?php echo $ds_lang[$ds_dl]["busqueda_sin_resultados"]; ?></div>
<?php } ?>
