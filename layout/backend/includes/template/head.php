<meta charset="iso-8859-1" />
<title>Admin | <?php echo ADMIN_NAME; ?></title>
<meta name="description" content="<?php echo lang("description"); ?>" />
<meta name="keywords" content="<?php echo lang("keywords"); ?>" />
<?php if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
    header('X-UA-Compatible: IE=edge,chrome=1');
} ?>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if IE]><script src="<?php echo ADMIN_URL; ?>lib/html5-ie/trunk.js"></script><![endif]-->
<link rel="stylesheet" href="<?php echo ADMIN_URL; ?>lib/fancybox/jquery.fancybox.css" type="text/css" />
<link rel="stylesheet" href="<?php echo ADMIN_URL; ?>lib/jQuery/ui-1.11.4/jquery-ui.structure.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo ADMIN_URL; ?>lib/jQuery/ui-1.11.4/jquery-ui.theme.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo COMMON_URL.minify("style", "css", ADMIN_DEVELOPMENT) ?>?ver=<?php echo ADMIN_VERSION; ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo ADMIN_THEME_URL; ?>style/<?php echo minify("main", "css", ADMIN_DEVELOPMENT) ?>?v=<?php echo ADMIN_VERSION; ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo ADMIN_URL; ?>lib/chosen/<?php echo minify("chosen", "css", ADMIN_DEVELOPMENT) ?>">