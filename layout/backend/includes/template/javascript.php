<?php
if(!isset($seccion)){
    $seccion = $sec;
}
?>
<script type="text/javascript">
    var SITE_URL = "<?php echo SITE_URL; ?>";
    var SITE_NAME = "<?php echo SITE_NAME; ?>";
    var ADMIN_URL = "<?php echo ADMIN_URL; ?>";
    var ADMIN_THEME_URL = "<?php echo ADMIN_THEME_URL; ?>";
    var UPLOAD_URL = "<?php echo UPLOAD_URL; ?>";
    var UPLOAD_PATH = "<?php echo UPLOAD_PATH; ?>";
    
    var SEC = "<?php echo $sec; ?>";
    var SECCION = "<?php echo $seccion; ?>";
    
    var ID = "<?php if (isset($id)) {echo $id;};?>";
    
    var CONFIRM_TXT = "<?php echo lang("confirm_txt"); ?>";
    var PAGINAS_BORRAR_ERROR = "<?php echo lang("paginas_borrar_error"); ?>";
    var LANG = "<?php echo LANG_DEFAULT; ?>";//DEL
    var IDIOMA_DEFECTO = "<?php echo $idiomas["defecto"]["idioma"]; ?>";
    var GRID = <?php echo SORTABLE_GRID_SIZE; ?>;
    var PAGINAS_OCULTAS_TXT = "<?php echo lang("paginas_ocultas_txt"); ?>";
    var TOGGLE_NAV_COOKIE = "<?php echo TOGGLE_NAV_COOKIE; ?>";
    var TOGGLE_LEGEND_COOKIE = "<?php echo TOGGLE_LEGEND_COOKIE; ?>";

    var BTN_ANYADIR = "<?php echo lang("anyadir"); ?>";
    var BTN_GUARDAR_VOLVER = "<?php echo lang("guardar_volver"); ?>";
    var ADMIN_SUB = "<?php echo $sub; ?>";

    var ELIMINAR_SUBCATEGORIAS_TXT = "<?php echo lang("eliminar_subcategorias_txt"); ?>";
    var ELIMINAR_CAT_ARTICULOS_TXT = "<?php echo lang("eliminar_cat_articulos_txt"); ?>";

    var cancelar_txt = "<?php echo lang("cancelar"); ?>";
</script>
<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/jQuery/jQuery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/chosen/<?php echo minify("chosen.jquery", "js", ADMIN_DEVELOPMENT); ?>"></script>
<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/chosen/docsupport/<?php echo minify("prism", "js", ADMIN_DEVELOPMENT); ?>" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/<?php echo minify("funciones", "js", ADMIN_DEVELOPMENT); ?>?v=<?php echo ADMIN_VERSION; ?>"></script>
<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/tablesorter/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_URL; ?>js/<?php echo minify("main", "js", ADMIN_DEVELOPMENT); ?>?v=<?php echo ADMIN_VERSION; ?>"></script>

<script type="text/javascript" src="<?php echo ADMIN_URL; ?>includes/plugins/seo/<?php echo minify("main", "js", ADMIN_DEVELOPMENT); ?>?v=<?php echo ADMIN_VERSION; ?>"></script>

<script type="text/javascript" src="<?php echo ADMIN_URL; ?>js/<?php echo minify("paginas", "js", ADMIN_DEVELOPMENT); ?>?v=<?php echo ADMIN_VERSION; ?>"></script>
<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/fran6validate/<?php echo minify("fran6validate", "js", ADMIN_DEVELOPMENT); ?>?v=<?php echo ADMIN_VERSION; ?>"></script>
<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/tinyValidate/<?php echo minify("tinyValidate", "js", ADMIN_DEVELOPMENT); ?>?v=<?php echo ADMIN_VERSION; ?>"></script>

<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/ckeditor/adapters/jquery.js"></script>

<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo ADMIN_URL; ?>lib/jQuery/ui-1.11.4/jquery-ui.min.js"></script>