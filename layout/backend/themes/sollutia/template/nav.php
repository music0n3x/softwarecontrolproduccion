<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li> <a class="waves-effect waves-dark" href="<?php echo Util::generateRoute("home", "list"); ?>"><i class="fa fa-project-diagram"></i><span class="hide-menu"><?php echo $langXML->projects; ?></span></a>
        </li>
        <li> <a class="waves-effect waves-dark" href="<?php echo Util::generateRoute("admins", "list"); ?>" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu"><?php echo $langXML->clients; ?></span></a>
        </li>
        <li> <a class="has-arrow waves-effect waves-dark" aria-expanded="false"><i id="clipboardicon" class="fas fa-clipboard-list"></i><span class="hide-menu"><?php echo $langXML->logs; ?></span></a>
            <ul aria-expanded="true" class="collapse">
                <li><a href="<?php echo Util::generateRoute("log", "logByTechnician"); ?>"><?php echo $langXML->byTechnician; ?> </a></li>
                <li><a href="<?php echo Util::generateRoute("log", "logByProject"); ?>"><?php echo $langXML->byProject; ?></a></li>
                <li><a href="<?php echo Util::generateRoute("log", "logByDate"); ?>"><?php echo $langXML->byDate; ?></a></li>
            </ul>
        </li>
    </ul>
</nav>