<script src = "/layout/backend/themes/sollutia/template/assets/node_modules/jquery/jquery.min.js"></script>
<script src="/layout/backend/themes/sollutia/template/assets/node_modules/bootstrap/js/popper.min.js"></script>
<script src="/layout/backend/themes/sollutia/template/assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
<script src="/layout/backend/themes/sollutia/template/html/js/perfect-scrollbar.jquery.min.js"></script>
<script src="/layout/backend/themes/sollutia/template/html/js/waves.js"></script>
<script src="/layout/backend/themes/sollutia/template/html/js/sidebarmenu.js"></script>
<script src="/layout/backend/themes/sollutia/template/html/js/custom.min.js"></script>
<script src="/layout/backend/themes/sollutia/template/assets/node_modules/raphael/raphael-min.js"></script>
<script src="/layout/backend/themes/sollutia/template/assets/node_modules/morrisjs/morris.min.js"></script>
<script src="/layout/backend/themes/sollutia/template/assets/node_modules/d3/d3.min.js"></script>
<script src="/layout/backend/themes/sollutia/template/assets/node_modules/c3-master/c3.min.js"></script>
<script src="/layout/backend/themes/sollutia/template/html/js/dashboard1.js"></script>
<script src="/layout/backend/section/login/js/validaForm.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js"></script>
<script src="/layout/backend/themes/sollutia/template/html/js/danielLoader.js"></script>


<script type="text/javascript">
    $(document).ready(function () {
        $.fn.dataTable.moment('DD/MM/YYYY');

        $('#dataTable').DataTable({
            "order": [[2, "desc"]],
            "language": {
                "decimal": "",
                "emptyTable": "<?php echo $langXML->emptyTable; ?>",
                "info": "<?php echo $langXML->dataTableInfo; ?>",
                "infoEmpty": "<?php echo $langXML->infoEmpty; ?>",
                "infoFiltered": "<?php echo $langXML->infoFiltered; ?>",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "<br><br><?php echo $langXML->lengthMenu; ?>",
                "loadingRecords": "<?php echo $langXML->loadingRecords; ?>",
                "processing": "<?php echo $langXML->processing; ?>",
                "search": "<?php echo $langXML->dataTableSearch; ?><br>",
                "zeroRecords": "<?php echo $langXML->zeroRecords; ?>",
                "paginate": {
                    "first": "<?php echo $langXML->first; ?>",
                    "last": "<?php echo $langXML->last; ?>",
                    "next": "<?php echo $langXML->next; ?>",
                    "previous": "<?php echo $langXML->previous; ?>"
                },
                "aria": {
                    "sortAscending": "<?php echo $langXML->sortAscending; ?>",
                    "sortDescending": "<?php echo $langXML->sortDescending; ?>"
                }
            }
        });

        $('.dataTables_filter').addClass('datatableSearch');
        $('.dataTables_length').addClass('datatableShow');
        $('#dataTable_wrapper').prepend('<div id="datatableTopDiv"></div>');
        $('#datatableTopDiv').append($('#dataTable_filter'));
        $('#datatableTopDiv').append($('#dataTable_length'));

        $('.dataTables_filter').removeClass('dataTables_filter');
        $('.dataTables_length').removeClass('dataTables_length');
        $('input').addClass('searchButton');

        $('.onclicktr .clickable').click(function () {
            var href = $(this).find("a").attr("href");
            if (href) {
                window.location = href;
            }
        });
        
        $('.searchButton').focusout(function () {
            $('.onclicktr .clickable').click(function () {
                var href = $(this).find("a").attr("href");
                if (href) {
                    window.location = href;
                }
            });
        });

        $('.dataTables_wrapper').click(function () {
            $('.onclicktr .clickable').click(function () {
                var href = $(this).find("a").attr("href");
                if (href) {
                    window.location = href;
                }
            });
        });

    });

</script>