<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php?<?php echo Util::calculateLang(); ?>">
                <b>
                    <img src="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/assets/images/logoSollutia40.png" alt="homepage" class="dark-logo" />
                    <img src="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                </b>
                <span>
                    <img src="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/assets/images/logoSollutiaText.png" alt="homepage" class="dark-logo" />
                    <img src="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
        </div>
        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="fa fa-bars"></i></a> </li>
                <li>
                    <?php
                    foreach ($breadcrumb as $index => $value) {
                        if ($index == 0) {
                            ?>
                            <a id="changeLanguageHeader" href="<?php echo $value['href']; ?>"><?php echo $value['label']; ?></a>
                        <?php } else { ?>
                            &nbsp;&nbsp;<i class="fas fa-arrow-circle-right"></i>&nbsp;&nbsp;<a id="breadCrumb" href="<?php echo $value['href']; ?>"><?php echo $value['label']; ?></a> 
                        <?php } ?>
                    <?php }
                    ?>
                </li>
            </ul>
            <ul class="navbar-nav my-lg-0">
                <li class="nav-item dropdown u-pro">
                    <div class="notClickable nav-link dropdown-toggle waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-circle"></i> <span class="hidden-md-down"><?php echo TechnicianController::getTechnicianName($_SESSION['id']); ?> &nbsp;</span> </div>
                </li>
                <li class="logoutLiHeader nav-item"><a class="logoutAHeader nav-link dropdown-toggle waves-effect" href="<?php echo Util::generateRoute("login", "logout"); ?>"><?php echo $langXML->logout; ?></a></li>
                <?php if ($sec=="log" || $act=="list"){ ?><li class="logoutLiHeader nav-item"><a class="logoutAHeader nav-link dropdown-toggle waves-effect" href="<?php echo Util::generateRouteChangeLang($sec, $act); ?>"><?php echo $langXML->changeLanguage; ?></a></li> <?php } ?>
            </ul>
        </div>
    </nav>
</header>