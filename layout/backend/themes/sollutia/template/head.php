<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="software de control de la producción de sollutia">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/assets/images/favicon.ico">
    <title><?php echo $actTitle; ?> - <?php echo $langXML->productionControl; ?></title>
    <link type="text/css" rel="stylesheet" href="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/html/css/fran6trap.css">
    <link type="text/css" rel="stylesheet" href="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/html/css/main.css">
    <link type="text/css" href="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/html/css/style.css" rel="stylesheet">
    <link type="text/css" href="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/html/css/colors/default.css" id="theme" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" id="theme" rel="stylesheet">
</head>