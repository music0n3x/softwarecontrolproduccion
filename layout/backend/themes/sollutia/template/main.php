<!DOCTYPE html>
<html lang="en">
    <?php include "head.php"; ?>
    <body class="fix-header fix-sidebar card-no-border">
        <div id="loaderDaniel" class="preloader">
            <div class="loader">
                <div class="loader__figure"></div>
                <p class="loader__label"><?php echo $langXML->loading; ?></p>
            </div>
        </div>
        <div id="main-wrapper">
            <?php include "header.php"; ?>
            <aside class="left-sidebar">
                <div class="scroll-sidebar">
                    <?php include "nav.php"; ?>
                </div>
            </aside> 
            <div class="page-wrapper">
                <div class="container-fluid">
                    <?php include "/home/webspace/controlproduccio2/html/layout/backend/section/$sec/$view"; ?>
                </div>
            </div>
        </div>
        <?php include "javascript.php"; ?>
    </body>
</html>