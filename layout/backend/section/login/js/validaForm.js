function validaCampo(campo) {

    campo.setCustomValidity("");
    if (campo.checkValidity()) {
        return true;
    } else {
        if (campo.validity.valueMissing) {
            campo.setCustomValidity("Este campo es obligatorio");
            pintaError(campo);
        }
    }

}

function pintaError(campo) {
    let errorMsg = campo.validationMessage;
    let div = document.createElement("div");
    div.className = "errorDiv";
    div.innerHTML = errorMsg;

    insertAfter(div, campo.parentNode);


}

function resetError(campo) {

    let errores = campo.getElementsByClassName("errorDiv");

    for (var i = errores.length - 1; i >= 0; --i) {
        errores[i].remove();
    }

}

function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

let form = document.getElementById("loginForm");
form.addEventListener('submit', function () {
    event.preventDefault();
    resetError(form);

    let username = document.getElementById("username");
    let password = document.getElementById("password");


    if (form.checkValidity()) {
        document.forms["loginForm"].submit();
    } else {
        validaCampo(username);
        validaCampo(password);
    }



});