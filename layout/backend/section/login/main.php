<?php

switch ($act) {
    case "loginForm":
        $view = "view/$act.php";
        include LOGIN_HANDLER;
        break;
    case "loginCheck":
        include "controller/$act.php";
        break;
    case "logout":
        include "controller/$act.php";
        Util::redirect("?sec=login&act=loginForm&lang=$lang");
        break;
}