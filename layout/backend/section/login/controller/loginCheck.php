<?php

if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    Util::redirect("index.php?lang=$lang");
} else {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $technicianController = new TechnicianController();
    $technician = $technicianController->getTechnicianByUsername($username);
    if (!empty($technician)) {
        if (md5($password) == $technician['password']) {
            $_SESSION["loggedin"] = TRUE;
            $_SESSION["id"] = $technician['id'];
            $_SESSION["username"] = $username;
            Util::redirect(Util::generateRoute("home", "list"));
        } else {
            Util::redirect(Util::generateRoute("login", "loginForm" . "&error=true"));
        }
    } else {
        Util::redirect(Util::generateRoute("login", "loginForm" . "&error=true"));
    }
}