<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Control de producción</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link type="text/css" rel="stylesheet" href="/layout/backend/themes/sollutia/template/html/css/main.css">

        <!--===============================================================================================-->	
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo SITE_URL;?>layout/backend/themes/sollutia/template/assets/images/favicon.ico">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="/layout/backend/section/login/assets/vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="/layout/backend/section/login/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="/layout/backend/section/login/assets/fonts/iconic/css/material-design-iconic-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="/layout/backend/section/login/assets/vendor/animate/animate.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="/layout/backend/section/login/assets/vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="/layout/backend/section/login/assets/vendor/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="/layout/backend/section/login/assets/vendor/select2/select2.min.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="/layout/backend/section/login/assets/vendor/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="/layout/backend/section/login/assets/css/util.css">
        <link rel="stylesheet" type="text/css" href="/layout/backend/section/login/assets/css/main.css">
        <!--===============================================================================================-->
    </head>
    <body>

        <div class="limiter">
            <div class="container-login100">
                <div id="loginCornerDiv">
                    <?php if ($lang == "es") { ?>
                        <a id="aChangeLanguage" href="?sec=login&act=loginForm&lang=en"><b><?php echo $langXML->changeLanguage; ?></b></a> 
                    <?php } else { ?>
                        <a id="aChangeLanguage" href="?sec=login&act=loginForm&lang=es"><b><?php echo $langXML->changeLanguage; ?></b></a> 
                    <?php } ?>
                </div>
                <div class="wrap-login100">
                    <form class="login100-form" id="loginForm" action="<?php echo Util::generateRoute("login", "loginCheck"); ?>" method="post" novalidate>
                        <span class="login100-form-title p-b-26">
                            <?php echo $langXML->productionControl; ?>
                        </span>
                        <span class="login100-form-title p-b-48">
                            <img src="/layout/backend/section/login/assets/images/logoSollutia.png">
                        </span>
                        <?php if (isset($error) && $error) { ?>
                            <span class="login100-form-title p-b-48">
                                <h4 id="error"><?php echo $langXML->error; ?></h4>
                            </span>
                        <?php } ?>
                        <div class="wrap-input100" >
                            <input class="input100" type="text" name="username" id="username" required>
                            <span class="focus-input100" data-placeholder="<?php echo $langXML->username; ?>"></span>
                        </div>
                        <div class="wrap-input100">
                            <span class="btn-show-pass">
                                <i class="zmdi zmdi-eye"></i>
                            </span>
                            <input class="input100" type="password" name="password" id="password" required>
                            <span class="focus-input100" data-placeholder="<?php echo $langXML->password; ?>"></span>
                        </div>
                        <input id="loginButton" type="submit" class="btn btn-primary" value="<?php echo $langXML->login; ?>">     
                    </form>
                </div>
            </div>
        </div>


        <!--===============================================================================================-->
        <script src="/layout/backend/section/login/assets/vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="/layout/backend/section/login/assets/vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="/layout/backend/section/login/assets/vendor/bootstrap/js/popper.js"></script>
        <script src="/layout/backend/section/login/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="/layout/backend/section/login/assets/vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="/layout/backend/section/login/assets/js/main.js"></script>
        <script src="/layout/backend/section/login/js/validaForm.js"></script>
    </body>
</html>