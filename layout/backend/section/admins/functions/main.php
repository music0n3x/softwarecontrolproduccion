<?php

function anyadir_guardar_admin($title, $mail, $usuario, $password) {
    global $link;

    $sql = "INSERT INTO admins (title, mail, usuario, pass, activo) VALUES ('$title','$mail','$usuario',MD5('$password'),1)";
    mysqli_query($link, $sql);
    $id = mysqli_insert_id($link);
    
    insert_log("admins", $id, array("title"=>$title,"mail"=>$mail,"usuario"=>$usuario, "password"=>md5($password)), "insert");
    
    return $id;
}

function editar_guardar_admin($title, $mail, $usuario, $password, $id) {
    global $link;

    if ($password != "") {
        $sql = "UPDATE admins SET title='$title', mail='$mail', usuario='$usuario', pass=MD5('$password'), activo=1 WHERE id = $id";
    } else {
        $sql = "UPDATE admins SET title='$title', mail='$mail', usuario='$usuario', activo=1 WHERE id = $id";
    }
//    echo $sql;
    mysqli_query($link, $sql);
    
    if($password == "d41d8cd98f00b204e9800998ecf8427e"){
        $password = "";
    }
    insert_log("admins", $id, array("title"=>$title,"mail"=>$mail,"usuario"=>$usuario, "password"=>$password), "update");
}

function eliminar_admin($id) {
    global $link;

    $sql = "DELETE FROM admins WHERE id = $id";
    mysqli_query($link, $sql);
    
    $sql_sec = "DELETE FROM admins_secciones WHERE admin = $id";
    mysqli_query($link, $sql_sec);

    
    insert_log("admins", $id, NULL, "delete");
}

/* ------------------------------------------------------------------ load -- */

function load_admins_secciones($link, $admin_id, $admin_root) {
    $query = "SELECT * FROM config_secciones WHERE activo = 1 AND seccion <> 'defecto' AND admin = 1 ";
    if ($admin_root == 1) {
        $query .= " AND admin_sec <> 'admins'";
    }
//    echo $query."<br />";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $princ = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $aux = array();
            $checked = "";
            if (activar_seccion($admin_id, $row['id'])) {
                $checked = 'checked="checked"';
            }
            $aux["id"] = $row["id"];
            $aux["content"] = lang("sec_" . $row["admin_sec"]);
            $aux["checked"] = $checked;

            $princ[] = $aux;
        }
        return $princ;
    }
    return false;
}

function load_admins_list($link, $search_term) {
    $query = "SELECT * FROM admins WHERE id <> 0";
    if ($search_term != "") {
        $query .= " AND (title LIKE '%" . $search_term . "%' OR mail LIKE '%" . $search_term . "%' OR usuario LIKE '%" . $search_term . "%')";
    }
    $query .= " ORDER BY title ASC";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $i = 1;
        $princ = array();
        while ($row = mysqli_fetch_array($result)) {
            $aux = array();
            $tipo = lang("usuario_administrador");
            $fecha_acceso = "-";
            if ($row["root"]) {
                $tipo = lang("usuario_super_administrador");
            }
            if ($row['fecha_acceso'] != "0000-00-00") {
                $fecha_acceso = fecha_to_view($row['fecha_acceso']);
            }
            $aux["#"] = $i;
            $aux["link"] = ADMIN_URL . "index.php?sec=admins&sub=editar&id=" . $row["id"];
            $aux["delete_link"] = ADMIN_URL . "index.php?sec=admins&sub=eliminar&id=" . $row["id"];
            $aux["id"] = $row["id"];
            $aux["tipo"] = $tipo;
            $aux["root"] = $row["root"];
            $aux["usuario"] = $row["usuario"];
            $aux["title"] = $row["title"];
            $aux["mail"] = $row["mail"];
            $aux["fecha_acceso"] = $fecha_acceso;
            $princ[] = $aux;
            $i++;
        }
        return $princ;
    }
    return false;
}

function load_admins_item($link, $id) {
    $query = "SELECT * FROM admins WHERE id=$id";
    $result = mysqli_query($link, $query);
    $nItems = mysqli_num_rows($result);
    if ($nItems > 0) {
        $row = mysqli_fetch_assoc($result);
        $princ = array(
            "id" => $row["id"],
            "root" => $row["root"],
            "title" => $row["title"],
            "mail" => $row["mail"],
            "usuario" => $row["usuario"]
        );
        return $princ;
    }
    return false;
}
