<?php

$clientId = $_GET['id'];
$client = $clientController->getClientInfo($clientId);
$name = $client['name'];
$info = $client['info'];

$actTitle = $langXML->editClient;

$first = Util::generateLabelAndRoute($langXML->clients, Util::generateRoute("admins", "list"));
$second = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act). "&id=$clientId");

$breadcrumb = Util::generateBreadCrumb($first, $second);