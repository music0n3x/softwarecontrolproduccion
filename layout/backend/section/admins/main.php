<?php

$clientController = new ClientController();
switch ($act) {
    case "list":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "singleClient":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "createClientForm":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "createClient":
        include "controller/$act.php";
        Util::redirect(Util::generateRoute("admins", "list"));
        break;
    case "editClientForm":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "editClient":
        include "controller/$act.php";
        Util::redirect(Util::generateRoute("admins", "list"));
        break;
    case "deleteClient":
        include "controller/$act.php";
        Util::redirect(Util::generateRoute("admins", "list"));
        break;
}