<form action="<?php echo Util::generateRoute("admins", "createClient"); ?>" method="POST">
    <!-- text -->
    <p>
        <label for="info"><?php echo $langXML->name; ?>: </label><br>
        <input type="text" class="selectWidth" name="name" value="" placeholder="name" required/>
    </p>

    <!-- textarea -->
    <p>
        <label for="info"><?php echo $langXML->info; ?>: </label><br>
        <textarea rows="5" cols="50" class="selectWidth" name="info" required></textarea>
    </p>

    <button class="mainButton" type="submit"><?php echo $langXML->create; ?></button>
</form>