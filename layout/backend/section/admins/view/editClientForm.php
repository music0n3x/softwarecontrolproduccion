<form action="<?php echo Util::generateRoute("admins", "editClient"); ?>" method="POST">
    <input type="hidden" name="id" value="<?php echo $clientId; ?>" />
    <!-- text -->
    <p>
        <label for="info"><?php echo $langXML->name; ?>: </label><br>
        <input type="text" class="selectWidth" name="name" value="<?php echo $name; ?>" required />
    </p>

    <!-- textarea -->
    <p>
        <label for="info"><?php echo $langXML->info; ?>: </label><br>
        <textarea rows="5" cols="50" class="selectWidth" name="info" required><?php echo $info; ?></textarea>
    </p>

    <button class="mainButton" type="submit"><?php echo $langXML->edit; ?></button>
</form>
