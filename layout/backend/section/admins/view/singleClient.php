<h3><?php echo $langXML->client; ?>:</h3><?php if (isset($client)) { ?>
<table>
    <thead>
        <tr><th><?php echo $langXML->name; ?></th><th><?php echo $langXML->info; ?></th></tr>
    </thead>
    <tbody>
        <tr><td><?php echo $client['name']; ?></td><td><?php echo $client['info']; ?></td></tr>
    </tbody>                
</table><?php } else { ?> <h2 class="error"><?php echo $langXML->errorMessage; ?></h2> <?php } ?>
