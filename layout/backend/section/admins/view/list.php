<table id="dataTable">
    <p>
        <a href="<?php echo Util::generateRoute("admins", "createClientForm"); ?>" class="button"><?php echo $langXML->insertClient; ?></a>
    </p>
    <thead>
        <tr class="table-header"><th><?php echo $langXML->name; ?></th><th><?php echo $langXML->info; ?></th><th></th></tr>
    </thead>
    <tbody>
        <?php
        foreach ($clientArray as $client) {
            ?>
            <tr class="onclicktr"> 
                <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("admins", "editClientForm"); ?>&id=<?php echo $client['id']; ?>'><?php echo $client['name']; ?></a></td>
                <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("admins", "editClientForm"); ?>&id=<?php echo $client['id']; ?>'><?php echo $client['info']; ?></a></td>                
                <td class="flexCenter">
                    <a class="aButton" href='<?php echo Util::generateRoute("admins", "deleteClient"); ?>&id=<?php echo $client['id']; ?>' onclick="return confirm('<?php echo $langXML->deleteConfirm; ?>')"><i class="fas fa-trash-alt"></i></a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
