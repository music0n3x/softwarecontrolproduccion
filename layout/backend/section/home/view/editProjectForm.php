<form action="<?php echo Util::generateRoute("home", "editProject"); ?>" method="POST">
    <p>
        <label for="client_id"><?php echo $langXML->technician; ?>:</label><br>
        <select name="client_id" class="selectWidth">
            <?php
            foreach ($clientsArray as $client) {
                ?>
                <option value="<?php echo $client['id'] ?>"<?php
                if ($client['id'] == $clientId) {
                    echo "selected";
                }
                ?> ><?php echo $client['name']; ?></option>
                        <?php
                    }
                    ?>
        </select>
    </p>
    <br>
    <input type="hidden" name="clientId" value="<?php echo $clientId; ?>">
    <input type="hidden" name="projectId" value="<?php echo $projectId; ?>">
    <!-- text -->
    <p>
        <label for="name"><?php echo $langXML->name; ?>: </label><br>
        <input type="text" name="name" class="selectWidth" value="<?php echo $name; ?>" required/>
    </p>
    <br>
    <!-- textarea -->
    <p>
        <label for="info"><?php echo $langXML->observations; ?>: </label><br>
        <textarea rows="5" cols="50" class="selectWidth" name="info" required><?php echo $info; ?></textarea>
    </p>
    <!-- date -->
    <p>
        <label for="date"><?php echo $langXML->createdDate; ?>: </label><br>
        <input type="date" class="dateInput" value="<?php echo $createdDate; ?>" name="createdDate" required>
    </p>

    <button class="mainButton" type="submit"><?php echo $langXML->edit; ?></button>
</form>
