<h3><?php echo $langXML->listTitle; ?></h3>
<table id="dataTable">
        <a href="<?php echo Util::generateRoute("home", "createProjectForm");; ?>" class="button"><?php echo $langXML->insertProject; ?></a>
    <thead>
        <tr class="table-header"><th><?php echo $langXML->name; ?></th><th><?php echo $langXML->info; ?></th><th class="dateCol"><?php echo $langXML->createdDate; ?></th><th></th></tr>
    </thead>
    <tbody>
        <?php
        foreach ($projectsArray as $project) {
            ?>
            <tr class="onclicktr"> 
                <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "singleProject"); ?>&id=<?php echo $project['id']; ?>'><?php echo $project['name']; ?></a></td>
                <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "singleProject"); ?>&id=<?php echo $project['id']; ?>'><?php echo $project['info']; ?></td>
                <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "singleProject"); ?>&id=<?php echo $project['id']; ?>'><?php echo Util::dateToSpanishFormat($project['created_date']); ?></td>
                <td class="flexCenter">
                    <a class="aButton" href='<?php echo Util::generateRoute("home", "editProjectForm"); ?>&id=<?php echo $project['id']; ?>'><i class="fa fa-edit"></i></a>
                    <a class="aButton" href='<?php echo Util::generateRoute("home", "deleteProject"); ?>&id=<?php echo $project['id']; ?>' onclick="return confirm('<?php echo $langXML->deleteConfirm; ?>')"><i class="fas fa-trash-alt"></i></a>
                </td>                
            </tr>
        <?php } ?>
    </tbody>
</table>
