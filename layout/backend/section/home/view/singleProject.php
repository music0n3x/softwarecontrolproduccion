<?php if (isset($project)) { ?>

<span class="inlineBox"><h2 class="h2SingleProject"><?php echo $project['name']; ?></h2> &nbsp;&nbsp; <h3 class="h3SingleProject"><?php echo $project['info']; ?></h3></span><br>
    <br>
    <p>
        <a href="<?php echo Util::generateRoute("home", "createTaskForm"); ?>&id=<?php echo $project['id']; ?>" class="button"><?php echo $langXML->insertTask; ?></a>
    </p>
    <h3><strong><?php echo $langXML->tasks; ?>:</strong></h3>
    <table>
        <thead>
            <tr class="table-header"><th><?php echo $langXML->title; ?></th><th><?php echo $langXML->hoursEstimation; ?></th><th><?php echo $langXML->hoursRealized; ?></th><th><?php echo $langXML->createdDate; ?></th><th><?php echo $langXML->startDate; ?></th><th><?php echo $langXML->finishDate; ?></th><th class="toolsCol"></th></tr>
        </thead>
        <tbody>
            <?php
            foreach ($projectTasks as $task) {
                ?><tr class="onclicktr">
                    <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "singleTask"); ?>&id=<?php echo $task['id']; ?>'><?php echo $task['title']; ?></a></td>
                    <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "singleTask"); ?>&id=<?php echo $task['id']; ?>'><?php echo $task['hours']; ?></a></td>
                    <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "singleTask"); ?>&id=<?php echo $task['id']; ?>'><?php echo TaskController::calculateHoursRealized($task['id']); ?></a></td>                    
                    <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "singleTask"); ?>&id=<?php echo $task['id']; ?>'><?php echo Util::dateToSpanishFormat($task['created_date']); ?></a></td>
                    <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "singleTask"); ?>&id=<?php echo $task['id']; ?>'><?php echo Util::dateToSpanishFormat($task['start_date']); ?></a></td>
                    <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "singleTask"); ?>&id=<?php echo $task['id']; ?>'><?php echo Util::dateToSpanishFormat($task['finish_date']); ?></a></td>
                    <td class="tdaligncenter"><a class="aButton" href='<?php echo Util::generateRoute("home", "editTaskForm"); ?>&id=<?php echo $task['id']; ?>'><i class="fa fa-edit"></i></a><a class="aButton" href='<?php echo Util::generateRoute("home", "deleteTask"); ?>&id=<?php echo $task['id']; ?>&projectId=<?php echo $project['id']; ?>' onclick="return confirm('<?php echo $langXML->deleteConfirm; ?>')"><i class="fas fa-trash-alt"></i></a></td>
                </tr>
            <?php } ?>
        </tbody>                
    </table>
<?php } else {
    ?><h2 class="error"><?php echo $langXML->errorMessageProject; ?></h2><?php }
?>
