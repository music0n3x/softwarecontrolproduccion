<form action="<?php echo Util::generateRoute("home", "createProject"); ?>" method="POST">
    <label for="client_id"><?php echo $langXML->technician; ?>:</label><br>
    <select name="client_id" class="selectWidth">
        <?php
        foreach ($clientsArray as $client) {
            ?>
            <option value="<?php echo $client['id'] ?>"><?php echo $client['name']; ?></option>
            <?php
        }
        ?>
    </select>
    <br><br>
    <!-- text -->
    <p>
        <label for="name"><?php echo $langXML->name; ?>: </label><br>
        <input type="text" class="selectWidth" name="name" value="" placeholder="nombre" required />
    </p>
    <br>
    <!-- textarea -->
    <p>
        <label for="observations"><?php echo $langXML->observations; ?>: </label><br>
        <textarea rows="5" cols="50" class="selectWidth" name="observations" required></textarea>
    </p>
    <button class="mainButton" type="submit"><?php echo $langXML->create; ?></button>
</form>
