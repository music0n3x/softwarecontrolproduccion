<form action="<?php echo Util::generateRoute("home", "editTechnicianTask"); ?>" method="POST">
    <legend><?php echo $langXML->legend; ?></legend>

    <!-- date -->
    <p>
        <label for="date"><?php echo $langXML->startDate; ?>: </label><br>
        <input type="date" class="dateInput" name="startDate" value="<?php echo $startDate; ?>" required />
    </p>

    <!-- int -->
    <p>
        <label for="minutes"><?php echo $langXML->minutes; ?>: </label><br>
        <input type="number" class="selectWidth" name="minutes" value="<?php echo $minutes; ?>" placeholder="hours" required />
    </p>

    <!-- textarea -->
    <p>
        <label for="info"><?php echo $langXML->observations; ?>: </label><br>
        <textarea rows="5" cols="50" class="selectWidth" name="description" required><?php echo $description; ?></textarea>
    </p>

    <!-- int -->
    <p>
        <input type="hidden" name="id" value="<?php echo $id; ?>" required />
    </p>

    <!-- int -->
    <p>
        <input type="hidden" name="taskId" value="<?php echo $taskId; ?>" required />
    </p>

    <!-- int -->
    <p>
        <input type="hidden" name="technicianId" value="<?php echo $technicianId; ?>" required />
    </p>    

    <button class="mainButton" type="submit"><?php echo $langXML->edit; ?></button>
</form>
