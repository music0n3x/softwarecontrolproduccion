<div class="whiteBackground divAdjustWidth">
    <p class="pTask"><strong><?php echo $langXML->title; ?></strong> : <?php echo $task['title']; ?></p>
    <p class="pTask"><strong><?php echo $langXML->info; ?></strong> : <?php echo $task['info']; ?></p>
    <p class="pTask"><strong><?php echo $langXML->hours; ?></strong> : <?php echo $task['hours']; ?></p>
    <p class="pTask"><strong><?php echo $langXML->createdDate; ?></strong> : <?php echo Util::dateToSpanishFormat($task['created_date']); ?></p>
    <p class="pTask"><strong><?php echo $langXML->startDate; ?></strong> : <?php echo Util::dateToSpanishFormat($task['start_date']); ?></p>
    <p class="pTask"><strong><?php echo $langXML->finishDate; ?></strong> : <?php echo Util::dateToSpanishFormat($task['finish_date']); ?></p>
</div>
    <p>
        <a href="<?php echo Util::generateRoute("home", "createTechnicianTaskForm"); ?>&id=<?php echo $projectId; ?>&taskId=<?php echo $task['id']; ?>" class="button"><?php echo $langXML->createTechnicianTask; ?></a>
    </p>
<br><h3><strong><?php echo $langXML->hours; ?></strong></h3><br>

<table>
    <thead>
        <tr class="table-header"><th class="dateCol"><?php echo $langXML->date; ?></th><th><?php echo $langXML->technician; ?></th><th><?php echo $langXML->description; ?></th><th><?php echo $langXML->minutes; ?></th><th></th></tr>
    </thead>
    <tbody>
        <?php foreach ($technicianTasksArray as $technicianTask) { ?>
            <tr class="onclicktr">
                <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "editTechnicianTaskForm"); ?>&id=<?php echo $technicianTask['id']; ?>&taskId=<?php echo $task['id']; ?>'><?php echo Util::dateToSpanishFormat($technicianTask['start_date']); ?></a></td>
                <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "editTechnicianTaskForm"); ?>&id=<?php echo $technicianTask['id']; ?>&taskId=<?php echo $task['id']; ?>'><?php echo TechnicianController::getTechnicianName($technicianTask['technician_id']); ?></a></td>
                <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "editTechnicianTaskForm"); ?>&id=<?php echo $technicianTask['id']; ?>&taskId=<?php echo $task['id']; ?>'><?php echo $technicianTask['description']; ?></a></td>
                <td class="clickable"><a class="colorBlack" href='<?php echo Util::generateRoute("home", "editTechnicianTaskForm"); ?>&id=<?php echo $technicianTask['id']; ?>&taskId=<?php echo $task['id']; ?>'><?php echo $technicianTask['minutes']; ?></a></td>
                <td class="tdaligncenter"><a href='<?php echo Util::generateRoute("home", "deleteTechnicianTask"); ?>&id=<?php echo $technicianTask['id']; ?>&taskId=<?php echo $task['id']; ?>' onclick="return confirm('<?php echo $langXML->deleteConfirm; ?>')"><i class="fas fa-trash-alt"></i></a></td></tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td class="borderNone"></td>
            <td class="borderNone"></td>
            <td class="borderNone"></td>
            <td><strong><?php echo $hours; ?>(<?php echo $langXML->hours; ?>)</strong></td>

        </tr>
    </tfoot>
</table>