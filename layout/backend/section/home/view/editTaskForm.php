<form action="<?php echo Util::generateRoute("home", "editTask"); ?>" method="POST">

    <input type="hidden" name="id" value="<?php echo $taskId; ?>">
    <input type="hidden" name="projectId" value="<?php echo $projectId; ?>">
    <input type="hidden" name="statusId" value="<?php echo $statusId; ?>">
    <input type="hidden" name="performanceId" value="<?php echo $performanceId; ?>">
    <input type="hidden" name="billingId" value="<?php echo $billingId; ?>">
  
    <!-- text -->
    <p>
        <label for="title"><?php echo $langXML->title; ?>: </label><br>
        <input type="text" class="selectWidth" name="title" value="<?php echo $title; ?>" required />
    </p>

    <!-- textarea -->
    <p>
        <label for="name"><?php echo $langXML->info; ?>: </label><br>
        <textarea rows="5" cols="50" class="selectWidth" name="info"><?php echo $info; ?></textarea>
    </p>
    
    <!-- number -->
    <p>
        <label for="hours"><?php echo $langXML->hours; ?>: </label><br>
        <input type="number" class="selectWidth" name="hours" value="<?php echo $hours; ?>" required />
    </p>

    <!-- date -->
    <p>
        <label for="createdDate"><?php echo $langXML->createdDate; ?>: </label><br>
        <input type="date" class="dateInput" value="<?php echo $createdDate; ?>" name="createdDate" required>
    </p>

    <!-- date -->
    <p>
        <label for="startDate"><?php echo $langXML->startDate; ?>: </label><br>
        <input type="date" class="dateInput" value="<?php echo $startDate; ?>" name="startDate" required>
    </p>

    <!-- date -->
    <p>
        <label for="finishDate"><?php echo $langXML->finishDate; ?>: </label><br>
        <input type="date" class="dateInput" value="<?php echo $finishDate; ?>" name="finishDate" required>
    </p>

    <!-- selects -->
    <p>
        <label for="status"><?php echo $langXML->status; ?>: </label><br>
        <select name="statusId" class="selectWidth">
            <?php
            foreach ($statusArray as $status) {
                ?>
                <option value="<?php echo $status['id'] ?>"<?php
                if ($status['id'] == $statusId) {
                    echo "selected";
                }
                ?> >     <?php echo $status['description']; ?></option>
                    <?php } ?>
        </select>
    </p>
    <p>
        <label for="performance"><?php echo $langXML->performance; ?>: </label><br>
        <select name="performanceId" class="selectWidth">
            <?php
            foreach ($performanceArray as $performance) {
                ?>
                <option value="<?php echo $performance['id'] ?>"<?php
                if ($performance['id'] == $performanceId) {
                    echo "selected";
                }
                ?> >     <?php echo $performance['description']; ?></option>
                    <?php } ?>
        </select>
    </p>
    <p>
        <label for="billing"><?php echo $langXML->billing; ?>: </label><br>
        <select name="billingId" class="selectWidth">
            <?php
            foreach ($billingArray as $billing) {
                ?>
                <option value="<?php echo $billing['id'] ?>"<?php
                if ($billing['id'] == $billingId) {
                    echo "selected";
                }
                ?> >     <?php echo $billing['description']; ?></option>
                    <?php } ?>
        </select>
    </p>
    <br>
    <button class="mainButton" type="submit"><?php echo $langXML->edit; ?></button>

</form>