<form action="<?php echo Util::generateRoute("home", "createTask") . "&id=" . $project['id']; ?>" method="POST">
    <legend><?php echo $langXML->legend; ?></legend>

    <!-- text -->
    <p>
        <label for="title"><?php echo $langXML->title; ?>: </label><br>
        <input type="text" class="selectWidth" name="title" value="" placeholder="title" required />
    </p>

    <!-- textarea -->
    <p>
        <label for="name"><?php echo $langXML->info; ?>: </label><br>
        <textarea rows="5" cols="50" class="selectWidth" name="info"></textarea>
    </p>

    <!-- int -->
    <p>
        <label for="hours"><?php echo $langXML->hours; ?>: </label><br>
        <input type="number" class="selectWidth" name="hours" value="" placeholder="hours" required /><br>
    </p>

    <!-- date -->
    <p>
        <label for="createdDate"><?php echo $langXML->createdDate; ?>: </label><br>
        <input type="date" class="dateInput" name="createdDate" value="" required /><br><br>


        <!-- date -->

        <label for="startDate"><?php echo $langXML->startDate; ?>: </label><br>
        <input type="date" class="dateInput" name="startDate" value="" required /><br><br>


        <!-- date -->

        <label for="finishDate"><?php echo $langXML->finishDate; ?>: </label><br>
        <input type="date" class="dateInput" name="finishDate" value="" required />
    </p>

    <!-- selects -->
    <p>
        <label for="status"><?php echo $langXML->status; ?>: </label><br>
        <select name="statusId" class="selectWidth">
            <?php
            foreach ($statusArray as $status) {
                ?>
                <option value="<?php echo $status['id'] ?>"><?php echo $status['description']; ?></option>
                <?php
            }
            ?>
        </select>
    </p>
    <p>
        <label for="performance"><?php echo $langXML->performance; ?>: </label><br>
        <select name="performanceId" class="selectWidth">
            <?php
            foreach ($performanceArray as $performance) {
                ?>
                <option value="<?php echo $performance['id'] ?>"><?php echo $performance['description']; ?></option>
                <?php
            }
            ?>
        </select>
    </p>
    <p>
        <label for="billing"><?php echo $langXML->billing; ?>: </label><br>
        <select name="billingId" class="selectWidth">
            <?php
            foreach ($billingArray as $billing) {
                ?>
                <option value="<?php echo $billing['id'] ?>"><?php echo $billing['description']; ?></option>
                <?php
            }
            ?>
        </select>
    </p>

    <!-- int -->
    <p>
        <input type="hidden" name="projectId" value="<?php echo $project['id']; ?>" placeholder="projectId"/>
    </p>
    <br>
    <button class="mainButton" type="submit"><?php echo $langXML->create; ?></button>
</form>
