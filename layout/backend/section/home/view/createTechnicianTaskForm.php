<form action="<?php echo Util::generateRoute("home", "createTechnicianTask"); ?>" method="POST">
    <legend><?php echo $langXML->legend; ?></legend>
    
    <!-- date -->
    <p>
        <label for="date"><?php echo $langXML->date; ?>: </label>
        <input type="date" class="dateInput" name="date" value="" required/>
    </p>
    
    <!-- int -->
    <p>
        <label for="minutes"><?php echo $langXML->minutes; ?>: </label>
        <input type="number" class="selectWidth" name="minutes" value="" placeholder="hours" required/>
    </p>
    
    <!-- textarea -->
    <p>
        <label for="info"><?php echo $langXML->observations; ?>: </label><br>
        <textarea rows="5" cols="50" class="selectWidth" name="info" required></textarea>
    </p>

    <!-- int -->
    <p>
        <input type="hidden" name="taskId" value="<?php echo $_GET['taskId']; ?>" />
    </p>
    
    <!-- int -->
    <p>
        <input type="hidden" name="projectId" value="<?php echo $_GET['projectId']; ?>" />
    </p>    

    <button class="mainButton" type="submit"><?php echo $langXML->create; ?></button>
</form>
