<?php

$taskController = new TaskController();
if (isset($_GET['id'])) {
    $project = $projectController->getProjectInfo($_GET['id']);
    $projectTasks = $taskController->getProjectTasks($project['id']);
}

$actTitle = $project['name'];

$first = Util::generateLabelAndRoute($langXML->projectsList, Util::generateRoute("home", "list"));
$second = Util::generateLabelAndRoute($project['name'], Util::generateRoute($sec, $act) . "&id=" . $project['id']);
$breadcrumb = Util::generateBreadCrumb($first, $second);
