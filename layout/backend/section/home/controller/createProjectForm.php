<?php

$clientController = new ClientController();
$clientsArray = $clientController->getAllClients();

$actTitle = $langXML->createProject;

$first = Util::generateLabelAndRoute($langXML->projectsList, Util::generateRoute("home", "list"));
$second = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act));

$breadcrumb = Util::generateBreadCrumb($first, $second);
