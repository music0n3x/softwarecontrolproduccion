<?php 

$taskController = new TaskController();
$projectController = new ProjectController();
$task = $taskController->getTask($_GET['id']);
$projectId = $task['project_id'];
$technicianTaskController = new technicianTaskController();
$technicianTasksArray = $technicianTaskController->getAllTechnicianTasksByTaskId($task['id']);
$minutes = 0;
foreach ($technicianTasksArray as $technicianTask){
    $minutes += $technicianTask['minutes'];
}
$hours = Util::convertToHoursMins($minutes);

$actTitle = $langXML->task;

$first = Util::generateLabelAndRoute($langXML->projectsList, Util::generateRoute("home", "list"));
$second = Util::generateLabelAndRoute($projectController->getProjectName($task['project_id']), Util::generateRoute("home", "singleProject") 
        . "&id=" . $task['project_id']);
$third = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act) . "&id=" . $task['id']);

$breadcrumb = Util::generateBreadCrumb($first, $second, $third);