<?php
$performanceController = new PerformanceController();
$billingController = new BillingController();
$statusController = new StatusController();
$projectController = new ProjectController();

$taskId = $_GET['id'];
$taskController = new TaskController();
$task = $taskController->getTask($taskId);
$info = $task['info'];
$title = $task['title'];
$hours = $task['hours'];
$createdDate = $task['created_date'];
$startDate = $task['start_date'];
$finishDate = $task['finish_date'];
$projectId = $task['project_id'];
$statusId = $task['status_id'];
$performanceId = $task['performance_id'];
$billingId = $task['billing_id'];

$performanceArray = $performanceController->getAllPerformances();
$billingArray = $billingController->getAllBillings();
$statusArray = $statusController->getAllStatus();

$actTitle = $langXML->editTask;

$first = Util::generateLabelAndRoute($langXML->projectsList, Util::generateRoute("home", "list"));
$second = Util::generateLabelAndRoute($projectController->getProjectName($projectId), Util::generateRoute("home", "singleProject") 
        . "&id=" . $projectId);

$third = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act) . "&id=" . $task['id']);

$breadcrumb = Util::generateBreadCrumb($first, $second, $third);
