<?php

$performanceController = new PerformanceController();
$billingController = new BillingController();
$statusController = new StatusController();

$project = $projectController->getProjectInfo($_GET['id']);

$performanceArray = $performanceController->getAllPerformances();
$billingArray = $billingController->getAllBillings();
$statusArray = $statusController->getAllStatus();

$actTitle = $langXML->createTask;

$first = Util::generateLabelAndRoute($langXML->projectsList, Util::generateRoute("home", "list"));
$second = Util::generateLabelAndRoute($project['name'], Util::generateRoute("home", "singleProject") . "&id=" . $project['id']);
$third = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act). "&id=" . $project['id']);

$breadcrumb = Util::generateBreadCrumb($first, $second, $third);
