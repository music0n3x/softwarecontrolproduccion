<?php

$actTitle = $langXML->createTechnicianTask;

$first = Util::generateLabelAndRoute($langXML->projectsList, Util::generateRoute("home", "list"));
$second = Util::generateLabelAndRoute($projectController->getProjectName($_GET['id']), Util::generateRoute("home", "singleProject") . "&id=" . $_GET['id']);
$third = Util::generateLabelAndRoute($langXML->task, Util::generateRoute("home", "singleTask") . "&id=" . $_GET['taskId']);
$fourth = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act) . "&id=" . $_GET['id'] . "&taskId=" . $_GET['taskId']);

$breadcrumb = Util::generateBreadCrumb($first, $second, $third, $fourth);
