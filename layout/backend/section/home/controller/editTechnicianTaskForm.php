<?php

$taskController = new TaskController();
$projectController = new ProjectController();
$technicianTaskController = new TechnicianTaskController();

$technicianTask = $technicianTaskController->getTechnicianTask($_GET['id']);
$startDate = $technicianTask['start_date'];
$minutes = $technicianTask['minutes'];
$description = $technicianTask['description'];
$technicianId = $technicianTask['technician_id'];
$taskId = $technicianTask['task_id'];
$id = $technicianTask['id'];

$task = $taskController->getTask($taskId);

$actTitle = $langXML->editTechnicianTask;

$first = Util::generateLabelAndRoute($langXML->projectsList, Util::generateRoute("home", "list"));
$second = Util::generateLabelAndRoute($projectController->getProjectName($task['project_id']), Util::generateRoute("home", "singleProject")
        . "&id=" . $task['project_id']);
$third = Util::generateLabelAndRoute($langXML->task, Util::generateRoute("home", "singleTask") . "&id=" . $taskId);
$fourth = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act) . "&id=" . $id . "&taskId=" . $taskId);

$breadcrumb = Util::generateBreadCrumb($first, $second, $third, $fourth);
