<?php

$projectId = $_GET['id'];
$project = $projectController->getProjectInfo($projectId);
$name = $project['name'];
$info = $project['info'];
$clientId = $project['client_id'];
$createdDate = $project['created_date'];
$clientController = new ClientController();
$clientsArray = $clientController->getAllClients();

$actTitle = $langXML->editProject;

$first = Util::generateLabelAndRoute($langXML->projectsList, Util::generateRoute("home", "list"));
$second = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act) . "&id=" . $projectId);

$breadcrumb = Util::generateBreadCrumb($first, $second);
