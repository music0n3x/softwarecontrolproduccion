<?php

$projectController = new ProjectController();
$taskController = new TaskController();
switch ($act) {
    case "list":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "singleProject":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "createTaskForm":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "createTask":
        include "controller/$act.php";
        Util::redirect(Util::generateRoute("home", "singleProject" . "&id=" . $_GET['id']));
        break;
    case "createProjectForm":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "createProject":
        include "controller/$act.php";
        Util::redirect(Util::generateRoute("home", "list"));
        break;
    case "editProjectForm":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "editProject":
        include "controller/$act.php";
        Util::redirect("?sec=home&act=list");
        break;
    case "editTaskForm":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "editTask":
        include "controller/$act.php";
        Util::redirect(Util::redirect(Util::generateRoute("home", "singleProject") . "&id=$id"));
        break;
    case "deleteProject":
        include "controller/$act.php";
        Util::redirect(Util::generateRoute("home", "list"));
        break;
    case "deleteTask":
        include "controller/$act.php";
        Util::redirect(Util::generateRoute("home", "singleProject") . "&id=$projectId");
        break;
    case "createTechnicianTaskForm":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "createTechnicianTask":
        include "controller/$act.php";
        Util::redirect(Util::generateRoute("home", "singleTask") . "&id=$taskId");
        break;
    case "singleTask":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "deleteTechnicianTask":
        include "controller/$act.php";
        Util::redirect(Util::generateRoute("home", "singleTask") . "&id=$taskId");
        break;
    case "editTechnicianTaskForm":
        include "controller/$act.php";
        $view = "view/$act.php";
        include TEMPLATE_ROUTE;
        break;
    case "editTechnicianTask":
        include "controller/$act.php";
        Util::redirect(Util::redirect(Util::generateRoute("home", "singleTask") . "&id=$taskId"));
        break;
}