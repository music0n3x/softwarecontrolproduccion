<form id="logForm" action="<?php echo Util::generateRoute("log", "logByProject"); ?>" method="post" >
    <label for="id"><?php echo $langXML->project; ?>:</label>
    <select name="id" class="selectWidth">
        <?php foreach ($projectsArray as $project) { ?>
            <option <?php
            if ($project['id'] == $projectId) {
                echo "SELECTED";
            }
            ?> value="<?php echo $project['id']; ?>"><?php echo $project['name']; ?></option>
                <?php
            }
            ?>
    </select>
    <input id="buttonDaniel" type="submit" value="Buscar">
</form><br>
<section>
    <?php if (isset($projectId)) { ?>
        <header>
            <h3 class="h1LogByProject"><?php echo $projectInfo['name']; ?></h3>
        </header><?php } ?>
    <?php
    if (isset($projectId)) {
        foreach ($logArray as $index => $log) {
            ?>
            <article>
                <br>
                <header>
                    <h4 class="h2LogByProject"><strong><?php echo $log['task']['title']; ?></strong></h4><p class="pLogByProject"> ( <?php echo $langXML->hoursEstimation; ?>: <?php echo $log['task']['hours']; ?> | <?php echo $langXML->hoursRealized; ?>: <?php echo TaskController::calculateHoursRealized($log['task']['id']); ?> ) </p>
                </header>
                <table class="tableDaniel whiteBackground">
                    <?php if ($index == 0) { ?>
                        <thead>
                            <tr id="grayBackground"><th class="dateCol"><?php echo $langXML->date; ?></th><th class="technicianCol"><?php echo $langXML->technician; ?></th><th><?php echo $langXML->description; ?></th><th class="timeCol"><?php echo $langXML->minutes; ?></th></tr>
                        </thead>
                    <?php } ?>
                    <?php foreach ($log['technicianTasks'] as $technicianTask) { ?>
                        <tr><td class="dateCol"><?php echo Util::dateToSpanishFormat($technicianTask['start_date']); ?></td><td class="technicianCol"><?php echo TechnicianController::getTechnicianName($technicianTask['technician_id']); ?></td><td><?php echo $technicianTask['description']; ?></td><td class="timeCol"><?php echo $technicianTask['minutes']; ?> min</td></tr>
                    <?php } ?>
                </table>
            </article>
            <br><br>
        <?php }
        ?> 
            <h4 class="error">Horas invertidas en el proyecto: <?php echo $hours; ?></h4>
    </section>
    <?php
} else {
    ?><h2 class="error"><?php echo $langXML->logByProjectMessage; ?></h2>
    </section><?php
}
?>
