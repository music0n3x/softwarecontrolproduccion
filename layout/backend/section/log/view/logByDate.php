<form id="logForm" action="<?php echo Util::generateRoute("log", "logByDate"); ?>" method="POST" >       
    <label for="date-start"><strong><?php echo $langXML->from; ?>:</strong></label>
    <input class="dateInput" name="date-start" type="date" value="<?php echo $startDate; ?>" required min="2000-01-01" max="<?php echo date("Y-m-d"); ?>">
    <label for="date-end"><strong><?php echo $langXML->to; ?>:</strong></label>
    <input class="dateInput" name="date-end" type="date" value="<?php echo $endDate; ?>" required min="2000-01-01" max="<?php echo date("Y-m-d"); ?>">
    <input id="buttonDaniel" type="submit" value="Buscar">    
</form>
<section>
    <?php
    if (isset($logByDateArray)) {
        foreach ($logByDateArray as $index => $fullProjectInfo) {
            ?>
            <br>
            <header>
                <br><h3 class="h1LogByDate"><?php echo $fullProjectInfo['projectName']; ?></h3>
            </header>
            <article>
                <?php
                foreach ($fullProjectInfo['taskInfo'] as $taskInfo) {
                    ?>       
                <br><h4 class="h2LogByDate"><strong><?php echo $taskInfo['taskTitle']; ?></strong></h4><p class="pLogByProject"> ( <?php echo $langXML->hoursRealized; ?>: <?php echo TaskController::calculateHoursRealizedDateRange($taskInfo['task_id'], $startDate, $endDate); ?> ) </p><br><br>

                    <table class="tableDaniel whiteBackground">
                        <?php
                        if ($index == 0) {
                            $index++;
                            ?>
                            <thead>
                                <tr id="grayBackground"><th class="dateCol"><?php echo $langXML->date; ?></th><th class="technicianCol"><?php echo $langXML->technician; ?></th><th><?php echo $langXML->description; ?></th><th class="timeCol"><?php echo $langXML->minutes; ?></th></tr>
                            </thead>
                        <?php } ?>
                        <tbody>
                            <?php foreach ($taskInfo['technicianTasks'] as $technicianTask) { ?>
                                <tr><td class="dateCol"><?php echo Util::dateToSpanishFormat($technicianTask['start_date']); ?></td><td class="technicianCol"><?php echo TechnicianController::getTechnicianName($technicianTask['technician_id']); ?></td><td><?php echo $technicianTask['description']; ?></td><td class="timeCol"><?php echo $technicianTask['minutes']; ?> min</td></tr>
                            <?php } ?>
                        </tbody>
                    </table><?php if ($index != 0) { ?><br><?php } ?>
                    <?php
                }
                ?>
            </article>
        <?php }
        ?>
    </section>
    <?php
} else {
    ?>
    <h2 class="error"><?php echo $langXML->message; ?></h2>
    </section>
    <?php
}
?>
