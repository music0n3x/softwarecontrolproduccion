<form id="logForm" action="<?php echo Util::generateRoute("log", "logByTechnician"); ?>" method="post" >
    <label for="id"><?php echo $langXML->technician; ?>:</label>
    <select name="id" class="selectWidth">
        <?php
        foreach ($techniciansArray as $technician) {
            ?><option <?php
            if ($technician['id'] == $technicianId) {
                echo "SELECTED";
            }
            ?> value="<?php echo $technician['id']; ?>"><?php echo $technician['name']; ?></option><?php
            }
            ?>
    </select>
    <input id="buttonDaniel" type="submit" value="<?php echo $langXML->search; ?>">
</form>
<section>
    <?php
    if (isset($technicianId)) {
        foreach ($arrayFullInfo as $index => $fullInfo) {
            ?>
            <br><br>
            <article>
                <header>
                    <h3 class="h1LogByTechnician"><?php echo $fullInfo['projectName']; ?></h3><br><br>                  
                </header>

                <article>
                    <?php foreach($fullInfo['taskInfo'] as $taskInfo){ ?>
                    <h4 class="h2LogByTechnician"><strong><?php echo $taskInfo['taskTitle']; ?></strong></h4><p class="pLogByProject"> (<?php echo $langXML->hoursRealized; ?>: <?php echo TaskController::calculateHoursRealizedByTechnician($taskInfo['task_id'], $technicianId); ?> ) </p><br><br><br>
                    <table class="tableDaniel whiteBackground">
                        <?php if ($index == 0) { $index++;?>
                            <thead>
                                <tr id="grayBackground"><th class="dateCol"><?php echo $langXML->date; ?></th><th class="technicianCol"><?php echo $langXML->technician; ?></th><th><?php echo $langXML->description; ?></th><th class="timeCol"><?php echo $langXML->minutes; ?></th></tr>
                            </thead>
                        <?php } ?>
                        <?php
                        foreach ($taskInfo['technicianTasks'] as $technicianTask) {
                            ?>
                            <tr><td class="dateCol"><?php echo Util::dateToSpanishFormat($technicianTask['start_date']); ?></td><td class="technicianCol"><?php echo TechnicianController::getTechnicianName($technicianTask['technician_id']); ?></td><td><?php echo $technicianTask['description']; ?></td><td class="timeCol"><?php echo $technicianTask['minutes']; ?> min</td></tr><?php
                        }
                        ?>
                    </table><br>
                    <br><h3><?php echo $langXML->hoursRealized; ?>:  <?php echo $logController->getTechnicianTaskByTaskAndTechnicianIdHours($technicianId, $taskInfo['task_id']); ?></h3><br><br>
                    <?php } ?>
                </article>
            </article>
            <br>
        <?php }
        ?>
        <h2 class="error"><?php echo $langXML->totalTechnicianHours; ?> <?php echo $technicianHours; ?></h2>
    </section>
    <?php
} else {
    ?>
    <br>
    <h2 class="error"><?php echo $langXML->logByProjectMessage; ?></h2>
    </section>
<?php } ?>