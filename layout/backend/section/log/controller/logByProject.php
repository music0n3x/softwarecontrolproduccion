<?php

$projectController = new ProjectController();

if (isset($_REQUEST['id'])) {
    $projectId = $_REQUEST['id'];
    $projectInfo = $projectController->getProjectInfo($projectId);
    $logArray = $logController->getProjectTasks($projectId);
    $hours = $logController->getProjectTasksHours($projectId);
}
$projectsArray = $projectController->getAllProjectsAlphabetically();

$actTitle = $langXML->logByProject;
$first = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act));

$breadcrumb = Util::generateBreadCrumb($first);