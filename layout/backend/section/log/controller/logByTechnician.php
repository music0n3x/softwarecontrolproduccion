<?php

$technicianController = new TechnicianController();
$logController = new LogController();
if (isset($_REQUEST['id'])) {
    $technicianId = $_REQUEST['id'];
    $arrayFullInfo = $logController->getLogByTechnician($technicianId);
    $technicianHours = $logController->getTechnicianHours($technicianId);
}
$technicianTaskController = new TechnicianTaskController();
$techniciansArray = $technicianController->getAllTechniciansAlphabetically();

$actTitle = $langXML->logByTechnician;
$first = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act));

$breadcrumb = Util::generateBreadCrumb($first);
