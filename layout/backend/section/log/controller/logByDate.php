<?php

$technicianTaskController = new TechnicianTaskController();
$logController = new LogController();
if (isset($_POST['date-start']) && isset($_POST['date-end']) && $_POST['date-start'] != '' && $_POST['date-end'] != '') {
    $startDate = $_POST['date-start'];
    $endDate = $_POST['date-end'];
    $logByDateArray = $logController->getLogByDate2($startDate, $endDate);
}

$actTitle = $langXML->logByDate;
$first = Util::generateLabelAndRoute($actTitle, Util::generateRoute($sec, $act));

$breadcrumb = Util::generateBreadCrumb($first);
